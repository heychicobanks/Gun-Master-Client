varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float u_time;

float noise(vec2 p){
    return fract(sin(dot(p ,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 rainbowColor(float value) {
    float r = sin(value * 6.28318530718 + 0.0) * 0.5 + 0.5;
    float g = sin(value * 6.28318530718 + 2.09439510239) * 0.5 + 0.5;
    float b = sin(value * 6.28318530718 + 4.18879020479) * 0.5 + 0.5;
    return vec3(r, g, b);
}

void main()
{
    vec4 col = texture2D(gm_BaseTexture, v_vTexcoord);
    
    float dist = distance(v_vTexcoord, vec2(0.5, 0.5));
    float n = noise(v_vTexcoord * 10.0 + vec2(u_time, u_time));
    
    if (dist < 0.5 && col.a > 0.0)
    {
        float glowIntensity = (0.5 - dist) * 2.0;
        col.rgb += rainbowColor(dist + n) * glowIntensity;
    }
    
    gl_FragColor = col * v_vColour;
}
