// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_profile(packet){
	if (packet.status.code != OK) {
		show_debug_message("Could not get profile information : " + packet.status.message);
		return;
	}
	global.client.username = packet.payload.username;
	global.client.level = packet.payload.level;
	global.client.experience = packet.payload.xp;
	global.client.character_id = packet.payload.characterId;
	
	show_debug_message("Profile retrieved successfully")
}