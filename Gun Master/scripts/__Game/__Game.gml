// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

enum CharacterType {
	PLAYER,
	BOT,
	REMOTE,
}

enum DefendState {
    DEFEND,
    DODGE,
    IDLE
}

enum GameMode {
    CUSTOM,
    STANDARD,
    RANKED
}

enum ConnectionType {
	TCP,
	UDP
}