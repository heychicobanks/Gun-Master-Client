// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function create_depth(character, range){
	var _factor = random_range(-range, range);
	static last_negative = false;
	
	_factor = (last_negative) ? abs(_factor) : -abs(_factor);
	last_negative = !last_negative;
	character.image_xscale = image_xscale * (1 + _factor);
	character.image_yscale = (1 + _factor);
}