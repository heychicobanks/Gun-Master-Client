// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function create_entity(parentId, entityId, type) {
    var entity;
	
	entity = undefined
	show_debug_message("parent id = " + parentId)
	show_debug_message("client id = " + global.client.id)
    switch (type) {
        case "cowboy":
			if (parentId != global.client.id) {
				// TODO : A modifier la gestion des id des personnages
				entity = create_character(0, x, y, global.game.position, CharacterType.REMOTE, true);
			} else {
				entity = create_character(0, x, y, global.game.self_position, CharacterType.PLAYER, true);
			}
            break;
		case "projectile":
			entity = instance_create_depth(-100, -100, 0, o_projectile_server);
			break;
		case "game_controller":
				entity = instance_create_depth(-100, -100, 0, c_timer)
			break;
		default:
			show_debug_message("Unknow entity type : " + string(type));
			break;
	   }
	if (entity) {
		entity.myId = entityId;
		show_debug_message("entity " + string(object_get_name(entity.object_index)) + " created");
	}
    return entity;
}