// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_dequeue(packet){
	if (packet.status.code != OK) {
		show_debug_message("Error while dequeuing : " + packet.status.message);
		return;
	}
	
	show_debug_message("Dequeued succesfully");

	global.client.state = ClientState.IDLE;
}