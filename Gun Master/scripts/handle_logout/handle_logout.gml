// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_logout(packet){
	if (packet.status.code != OK) {
		show_debug_message("Error while logging out : " + packet.status.message);
		return;
	}	

	show_debug_message("Log out succesfull");

	global.token = "";
	global.refreshToken = ""; // <--- il faut invalider le refreshToken dans le stockage
	
	global.client.id = undefined;
	
	// On retourne à la page de connexion
	room_goto(r_login);
}