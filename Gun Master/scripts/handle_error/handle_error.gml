// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_error(packet){
	show_debug_message("Error from server : " + packet.status.message);
}