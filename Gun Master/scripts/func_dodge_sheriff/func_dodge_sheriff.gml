// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_dodge_sheriff(character){
	if (!character._scr_dodge_checker(character)) {
		character._state = CharacterState.IDLE;
		return;
	}
	character.sprite_index = character._s_dodge;
	character.alarm[0] = character._dodge_frames;
}