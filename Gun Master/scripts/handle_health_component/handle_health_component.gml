// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_health_component(entity, healthData){	
	show_debug_message("hp : " + string(healthData.hp) + " max_hp : " + string(healthData.max_hp));
	entity._hp = healthData.hp;
	entity._max_hp = healthData.max_hp;
	//entity.last_update = current_time;
}