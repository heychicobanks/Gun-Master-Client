/// @function iglc_init();
/// @description Initialise the in-game log console
function iglc_init() {
	startX = 0;//the x coordinate of the left of the log console
	startY = 0;//the y coordinate of the top of the log console
	endX = startX+display_get_gui_width();//the x coordinate of the right of the log console
	endY = startY+round(display_get_gui_height()/4);//the y coordinate of the bottom of the log console
	backgroundCol = c_black;//log console colour
	backgroundAlpha = 0.3;//log console alpha
	textDefaultCol = c_white;//only used when writeType is set to false
	textAlpha = 0.9;
	font = fnt_log;
	maxWidthOffset = -font_get_size(font);//reduce the max-width text can be on a single line before a line break
	textVerticalSpacing = font_get_size(font)*2;//the vertical spacing between lines in the log console
	showConsoleOnLog = false;//whether to show the log console when a log is written
	if (showConsoleOnLog) {
		showConsoleOnLogTime = game_get_speed(gamespeed_fps)*3;//time in steps to show the log console when a log is written
		showConsoleOnLogAlarm = 0;//alarm number for showing the console on log write
	}
	showInGMSOutputWindowOnLog = true;//whether to show the log in the GMS Output Window when a log is written
	#region Define log prefixes
	writeDate = true;//whether to write the date the log was written
	if (writeDate) {
		yearFirst = false;
		monthBeforeDay = false;
		monthType = 1;//0=number, 1=abbreviated, 2=full word
		dateSeparator = "-";
	}
	writeTime = true;//whether to write the time the log was written
	if (writeTime) {
		AmPm12HourClock = true;//whether to write 24-hour clock (false) or 12-hour clock with am/pm (true)
		timeSeparator = ":";
	}
	writeObjectName = true;//whether to write the name of the object that wrote the log
	writeEvent = true;//whether to write the event the log was written from (define below). This requires putting iglc_set_event(); at the start of every event that uses iglc_write();
	writeType = true;//whether to write the type of the log (define below)
	#endregion
	if (writeEvent) {
		event = "";//don't change
		#region Define events
		//if you've set writeEvent to true, set-up your own events here
		events[3] = "KeyPress";
		events[2] = "Draw";
		events[1] = "Step";
		events[0] = "Create";
		#endregion
	}
	if (writeType) {
		#region Define log types
		#region Log types init (don't change)
		enum TYPE {
			name,
			col
		}
		#endregion
		//if you've set writeType to true, set-up your own log types here
		types[2][TYPE.col] = make_colour_rgb(208,0,0);
		types[2][TYPE.name] = "[FAIL]";
		types[1][TYPE.col] = c_green;
		types[1][TYPE.name] = "[SUCCESS]";
		types[0][TYPE.col] = c_white;
		types[0][TYPE.name] = "[INFO]";
		#endregion
	}
	#region Don't change
	global.logConsoleInstance = id;
	drawConsole = false;
	positionOffset = 0;
	iglcShowDismissShowing = false;
	if (os_browser == browser_not_a_browser) {//if not HTML5 then initialise the log array for the first 50 entries
		log[49][1] = "";
		log[49][0] = -1;
	}
	logSize = 0;
	#endregion
}

#region Log Console functions
/// @function iglc_set_event(event);
/// @description Set the event to write to the log console. Only required when writeEvent is set to true
/// @param {real} event The index of the event to use when writing to the log console (defined in iglc_init)
function iglc_set_event(_event) {
	with (global.logConsoleInstance) {
		if (writeEvent) {
			event = events[_event];
		}
	}
}

#region iglc_get_month_formatted
///@function iglc_get_month_formatted(month);
///@description Return the month formatted per iglc_init();
///@param {real} month The number of the month
function iglc_get_month_formatted(_month) {
	switch (monthType) {
		case 0:
			return string(_month);
		break;
		case 1:
			switch (_month) {
				case 1:
					return "Jan";
				break;
				case 2:
					return "Feb";
				break;
				case 3:
					return "Mar";
				break;
				case 4:
					return "Apr";
				break;
				case 5:
					return "May";
				break;
				case 6:
					return "Jun";
				break;
				case 7:
					return "Jul";
				break;
				case 8:
					return "Aug";
				break;
				case 9:
					return "Sep";
				break;
				case 10:
					return "Oct";
				break;
				case 11:
					return "Nov";
				break;
				case 12:
					return "Dec";
				break;
			}
		break;
		case 2:
			switch (_month) {
				case 1:
					return "January";
				break;
				case 2:
					return "February";
				break;
				case 3:
					return "March";
				break;
				case 4:
					return "April";
				break;
				case 5:
					return "May";
				break;
				case 6:
					return "June";
				break;
				case 7:
					return "July";
				break;
				case 8:
					return "August";
				break;
				case 9:
					return "September";
				break;
				case 10:
					return "October";
				break;
				case 11:
					return "November";
				break;
				case 12:
					return "December";
				break;
			}
		break;
	}
}
#endregion

/// @function iglc_write(type,string1,[string2,...,stringN]);
/// @description Write the given string(s) with the given log type to the log console
/// @param {real} type The index of the type of log being written (-1 if writeType is set to false)
/// @param {any} string1 The value to write to the log console (will be converted to string)
/// @param {any} [string2,...,stringN] Additional values to write to the log console (optional)
function iglc_write(_type,_string1) {
	with (global.logConsoleInstance) {
		var _logString = "";
		//add date
		if (writeDate) {
			if (yearFirst) {
				_logString += string(current_year)+dateSeparator;
				var _endDateString = " ";
			}
			else {
				var _endDateString = dateSeparator+string(current_year)+" ";
			}
			if (monthBeforeDay) {
				_logString += iglc_get_month_formatted(current_month)+dateSeparator;
				_logString += string(current_day);
			}
			else {
				_logString += string(current_day)+dateSeparator;
				_logString += iglc_get_month_formatted(current_month);
			}
			_logString += _endDateString;
		}
		//add time
		if (writeTime) {
			var _minute0 = "";
			if (current_minute < 10) {
				_minute0 = "0";
			}
			var _second0 = "";
			if (current_second < 10) {
				_second0 = "0";
			}
			if (AmPm12HourClock) {
				var _hour = current_hour;
				var _amPm = "AM";
				if (_hour > 12 || _hour == 0) {
					_hour = abs(_hour-12);
					if (_hour != 12) {
						_amPm = "PM";
					}
				}
				_logString += string(_hour)+timeSeparator+_minute0+string(current_minute)+timeSeparator+_second0+string(current_second)+_amPm+" ";
			}
			else {
				_logString += string(current_hour)+timeSeparator+_minute0+string(current_minute)+timeSeparator+_second0+string(current_second)+" ";
			}
		}
		//add object name
		if (writeObjectName) {
			_logString += "@"+object_get_name(other.object_index);
			if (writeEvent == false) {
				_logString += " ";
			}
		}
		//add event
		if (writeEvent) {
			_logString += ":"+event+ " ";
		}
		//add type
		if (writeType) {
			_logString += types[_type][TYPE.name]+" ";
		}
		//add log
		for (var _i=1; _i<argument_count; _i++) {
			if (_i == 1) {
				_logString += _string1;//stops "_string1 only referenced once" warning on function line
			}
			else {
				_logString += string(argument[_i]);
			}
		}
		log[logSize][0] = _type;
		log[logSize][1] = _logString;
		logSize += 1;
		positionOffset = 0;
		//show in GMS Output Window
		if (showInGMSOutputWindowOnLog) {
			show_debug_message(_logString);
		}
		//show console on log
		if (showConsoleOnLog == true && iglcShowDismissShowing == false) {
			drawConsole = true;
			alarm[showConsoleOnLogAlarm] = showConsoleOnLogTime;
		}
	}
}

/// @function iglc_showConsoleOnLog_alarm();
/// @description Dismisses the log console only when shown from on log write. Only required when showConsoleOnLog is set to true. Alarm number set in iglc_init();
function iglc_showConsoleOnLog_alarm() {
	if (drawConsole && iglcShowDismissShowing == false) {
		drawConsole = false;
		positionOffset = 0;
	}
}

/// @function iglc_show_dismiss();
/// @description Show or dismiss the in-game log console
function iglc_show_dismiss() {
	with (global.logConsoleInstance) {
		drawConsole = !drawConsole;
		iglcShowDismissShowing = drawConsole;
		positionOffset = 0;
	}
}

/// @function iglc_draw_gui();
/// @description Draw the in-game log console
function iglc_draw_gui() {
	if (!global.debug_mode)
		return;
	if (drawConsole == true) {
		//background
		draw_set_alpha(backgroundAlpha);
		draw_rectangle_color(startX,startY,endX,endY,backgroundCol,backgroundCol,backgroundCol,backgroundCol,0);
		//text
		draw_set_alpha(textAlpha);
		draw_set_colour(textDefaultCol);
		draw_set_valign(fa_bottom);
		var _lines = 0;
		for (var _i=logSize-1+positionOffset; _i>-1; _i--) {
			//stop drawing above the console
			if (endY-(_lines*textVerticalSpacing)-textVerticalSpacing < startY) {
				break;
			} 
			//colour
			var _type = log[_i][0];
			if (_type != -1) {
				draw_set_colour(types[_type][TYPE.col]);
			}
			//break text
			var _text = log[_i][1];
			var _lineBreaks = 0;
			if (string_width(_text) > (endX-startX)+maxWidthOffset) {
				var _lastSpacePos = -1;
				var _lineStart = 1;
				var _pos = 1;
				while (_pos < string_length(_text)) {
					//found space
					if (string_char_at(_text,_pos) = " ") {
						_lastSpacePos = _pos;
					}
					//new line
					if (string_width(string_copy(_text,_lineStart,max(1,_pos-_lineStart))) > (endX-startX)+maxWidthOffset) {
						if (_lastSpacePos != -1) {
							_text = string_insert("\n",_text,_lastSpacePos+1);
							_lineStart = _lastSpacePos+2;
							_lastSpacePos = -1;
						}
						else {
							_text = string_insert("\n",_text,_pos);
							_lineStart = _pos+1;
						}
						_lineBreaks += 1;
					}
					_pos += 1;
				}
			}
			//draw
			draw_text(startX,endY-(_lines*textVerticalSpacing),_text);
			_lines += _lineBreaks;
			_lines += 1;
		}
		draw_set_valign(fa_top);
		draw_set_alpha(1);
	}
}

/// @function iglc_move_up(amount);
/// @description Move up the log console by the input amount to show NEWER logs
/// @param {real} amount The amount to move up the log console
function iglc_move_up(_amount) {
	with (global.logConsoleInstance) {
		if (drawConsole) {
			if (positionOffset <= 0-_amount) {
				positionOffset += _amount;
			}
			else {
				positionOffset = 0;
			}
		}
	}
}

/// @function iglc_move_down(amount);
/// @description Move down the log console by the input amount to show OLDER logs
/// @param {real} amount The amount to move down the log console
function iglc_move_down(_amount) {
	with (global.logConsoleInstance) {
		if (drawConsole) {
			if (positionOffset >= -(logSize-1)+_amount) {
				positionOffset -= _amount;
			}
			else {
				positionOffset = -(logSize-1)
			}
		}
	}
}

/// @function iglc_save(fileLocationAndNameAndExtension);
/// @description Save the log console to a text file with the given file name and location
/// @param {string} fileLocationAndNameAndExtension The file name and location to save the text file
function iglc_save(_fileLocationAndNameAndExtension) {
	var _file = file_text_open_write(_fileLocationAndNameAndExtension);
	if (_file == -1) {
		return _file;
	}
	//init type counts
	if (writeType) { 
		var _typeCounts;
		_typeCounts[array_length(global.logConsoleInstance.types)-1] = 0;
	}
	//write logs
	for (var _i=0; _i<global.logConsoleInstance.logSize; _i++) {
		file_text_write_string(_file,global.logConsoleInstance.log[_i][1]);
		if (writeType) {
			_typeCounts[global.logConsoleInstance.log[_i][0]] += 1;
		}
		if (_i < global.logConsoleInstance.logSize-1) {
			file_text_writeln(_file);
		}
	}
	//write type counts
	if (writeType) {
		file_text_writeln(_file);
		file_text_writeln(_file);
		for (var _i=0; _i<array_length(global.logConsoleInstance.types); _i++) {
			file_text_write_string(_file,global.logConsoleInstance.types[_i][0]+": ");
			file_text_write_string(_file,_typeCounts[_i]);
			if (_i < array_length(global.logConsoleInstance.types)-1) {
				file_text_writeln(_file);
			}
		}
	}
	file_text_close(_file);
}
#endregion