// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_entities(packet){
	if (room != r_remote_arena_default) {
		show_debug_message("Not in game, cannot handle entities packet")
		return;
	}
	
	var entities = packet.payload.entities;
	var size = array_length(packet.payload.entities);
	
	for (var i = 0; i < size; i++) {
	    var entityData = entities[i];
		var parentId = entityData.parentId;
		var entityId = entityData.id;
		var type = entityData.type;
	    var components = entityData.components;
	    manage_entities(parentId, entityId, type, components);
	}
}