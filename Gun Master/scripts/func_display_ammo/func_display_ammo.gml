// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_display_ammo(_instance_ID, display_pos_x, display_pos_y, xoffset, _position){
	var mult;
	
	mult = _position;
	for (var i = 0; i < _instance_ID._max_ammo; i++)
		draw_sprite_ext(_instance_ID._ammo_sprite_index == -1 ? s_ammo : _instance_ID._ammo_sprite_index, 0, display_pos_x + (70 * mult) + (xoffset * i) * mult, display_pos_y + 192, 0.5, 0.5, 0, c_black, .3);
	for (var i = 0; i < _instance_ID._ammo; i++)
		draw_sprite_ext(_instance_ID._ammo_sprite_index == -1 ? s_ammo : _instance_ID._ammo_sprite_index, 0, display_pos_x + (70 * mult) + (xoffset * i) * mult, display_pos_y + 192,0.5, 0.5 ,0, image_blend, 1);
}