// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_special_attack_sheriff(character, isNetwork, isRemote = false) {
	var _can;
	
	_can = character._scr_special_attack_checker(character);
	
	character.sprite_index = character._s_special_attack;
	if (_can) {
		var random_index = irandom(ds_list_size(character._sfx_attack_normal) - 1);
		
		uc_shake(1, 2)
		instance_create_layer(x, y, "Instances", o_shell);
		if (!isRemote) {
			// On ne crée le projectile que pour le joueur local pour de la prédiction
			create_projectile(character, isNetwork);
		}
		character._ammo_slots[character._current_ammo_slot] -= 1;
		audio_play_sound(asset_get_index(character._sfx_attack_normal[| random_index]), 1, false);
	} else {
		var random_index = irandom(ds_list_size(character._sfx_attack_empty) - 1);
		audio_play_sound(asset_get_index(character._sfx_attack_empty[| random_index]), 1, false);
	}
}