// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_quick_attack_sheriff(character, isNetwork){
	var _can;
	static current_index = 0;
	
	_can = character._scr_quick_attack_checker(character)

	character.sprite_index = character._s_quick_attack;
	character.image_index = current_index;
	current_index = current_index <= character.image_number ? current_index + 1 : 0;
	

	if (_can) {
		var random_index = irandom(ds_list_size(character._sfx_attack_quick) - 1);
		
		uc_shake(1, 0.4)
		instance_create_layer(x, y, "Instances", o_shell);
		create_projectile(character, isNetwork);
		character._ammo_slots[character._current_ammo_slot] -= 1;
		audio_play_sound(asset_get_index(character._sfx_attack_quick[| random_index]), 1, false);
	} else {
		var random_index = irandom(ds_list_size(character._sfx_attack_empty) - 1);
		audio_play_sound(asset_get_index(character._sfx_attack_empty[| random_index]), 1, false);
	}
}
