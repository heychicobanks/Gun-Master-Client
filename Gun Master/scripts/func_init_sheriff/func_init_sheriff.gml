// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_init_sheriff(){
	_counter = false;
	
	_ammo_2 = 0;
	_current_ammo_slot = 0;
	
	_ammo_slots = [_ammo, _ammo_2];
}