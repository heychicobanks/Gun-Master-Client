// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function add_trail_sheriff(x1, y1, x2, y2, dpth, color){
	var t = instance_create_layer(x1, y1, "Instances", o_trail_sheriff);
	var l = point_distance(x1, y1, x2, y2);
	var d = point_direction(x1, y1, x2, y2);
	t.image_angle = d;
	t.image_xscale = l / 4;
	t.depth = dpth;
	t.image_blend = color;
}