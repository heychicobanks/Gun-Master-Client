function send_packet(data, socket, connectionType = ConnectionType.TCP){
	var data_buffer = buffer_create(1, buffer_grow, 1); // On crée le buffer
	
	SnapBufferWriteMessagePack(data_buffer, data) // Permet de sérialiser les données en binaire
	
	buffer_resize(data_buffer, buffer_tell(data_buffer));
	
	var _data_buffer_size = buffer_get_size(data_buffer); // On récupère la taille du buffer de données

	var final_buffer = buffer_create(_data_buffer_size  + 2, buffer_fixed, 1)
	
	buffer_write(final_buffer, buffer_u16, _data_buffer_size) // On ajout la taille du buffer de données
	buffer_copy(data_buffer, 0, _data_buffer_size, final_buffer, 2) // On copie

	if (connectionType == ConnectionType.TCP)
		var res = network_send_raw(socket, final_buffer, buffer_get_size(final_buffer))
	else
		var res = network_send_udp_raw(socket, global.game_socket.ip, global.game_socket.port, final_buffer, buffer_get_size(final_buffer)) 
	
	buffer_delete(data_buffer)
	buffer_delete(final_buffer)
	
	return (res);
}