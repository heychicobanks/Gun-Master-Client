// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

enum ComponentName {
  HEALTH,
  AMMO,
  DEFEND,
  POSITION,
  DASH,
  DODGE,
  VELOCITY,
  DAMAGE,
  SHOOT,
  HITBOX,
  TIMER,
  GAME_CONTROLLER,
  INPUT,
  COWBOY,
}

/***
 * Nom des entités dans le client
 */
enum EntityType {
  COWBOY,
  PROJECTILE,
  GAME_CONTROLLER, // Manager de jeu
}