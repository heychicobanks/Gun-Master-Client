// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function handle_position_component(entity, positionData){
		show_debug_message("position x : " + string(positionData.x) + " position y : " + string(positionData.y));				
		
		entity.server_x = positionData.x;
		entity.server_y = positionData.y;
		
		switch (entity.object_index) {
			case (o_player_network): {
				show_debug_message("Local player");
			
				var last_pos = ds_queue_dequeue(entity.position_buffer);		
				
				// Le buffer est vide
				if (!last_pos) {				
					entity.x = positionData.x;
					entity.y = positionData.y;
					
					entity.is_correcting = false;
					entity.last_update = current_time;				
				} else {
					// Les positions correspondent
					if (!(last_pos.x != positionData.x || last_pos.y != positionData.y)) {
						show_debug_message("Valid position");
						return;
					}
					// La position n'est pas bonne, on active l'interpolation				
					show_debug_message("Invalid position prediction");
					
					entity.x = positionData.x;
					entity.y = positionData.y;
					
					ds_queue_clear(entity.position_buffer);
				}
				break;
			}
			case (o_player_remote): {
				show_debug_message("Remote player");

				entity.x = positionData.x;
				entity.y = positionData.y;
				break;
			}
			default: {
				
				//entity.is_correcting = true;
				
				//entity.current_x = positionData.x;
				//entity.current_y = positionData.y;
				
				//entity.last_update = current_time;
				
				
				// On remet le buffer à zéro
				//ds_queue_clear(entity.position_buffer);
				entity.x = positionData.x;
				entity.y = positionData.y;

				break;
			}
		};		
}