// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_defend_sheriff(_character){
	if (!_character._scr_defend_checker(_character)) {
		_character._state = CharacterState.IDLE;
		return;
	}
	_character._defend += 1;
	_character.sprite_index = _character._s_defend;
}