// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_display_hp(_instance_ID, display_pos_x, display_pos_y, xoffset, _position){
	var mult;
	
	mult = _position
	for (var i = 0; i < _instance_ID._max_hp; i++)
		draw_sprite_ext(_instance_ID._hp_sprite_index == -1 ? s_hp : _instance_ID._hp_sprite_index, 5, display_pos_x + (70 * mult) + (xoffset * i) * mult, display_pos_y  + 60, 0.7 , 0.7, 0, c_black, .3);
	for (var i = 0; i < _instance_ID._hp; i++)
		draw_sprite_ext(_instance_ID._hp_sprite_index == -1 ? s_hp : _instance_ID._hp_sprite_index, 5, display_pos_x  + (70 * mult) + (xoffset * i) * mult, display_pos_y + 60, 0.7, 0.7, 0 , image_blend, 1);
}