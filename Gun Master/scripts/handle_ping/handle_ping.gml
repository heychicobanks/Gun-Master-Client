// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_ping(packet){
	send(PacketType.Pong, {"timestamp": packet.payload.timestamp});
}