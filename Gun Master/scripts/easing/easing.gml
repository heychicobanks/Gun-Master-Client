// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function easing(_current, _target, _type, _speed) {
	var delta = _target - _current;
	switch (_type) {
	    case "linear":
	        return _current + delta * _speed;
	    case "easeInQuad":
	        return _current + delta * power(_speed, 2);
	    case "easeOutQuad":
	        return _current - delta * (_speed * (_speed - 2) - 1);
	    case "easeInOutQuad":
	        _speed *= 2;
	        if (_speed < 1) return _current + delta / 2 * power(_speed, 2);
	        return _current - delta / 2 * ((_speed - 1) * (_speed - 3) - 1);
	    default:
	        return _current;
	}
}