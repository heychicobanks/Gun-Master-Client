// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function create_button(button_index, character_instance_id) {
	var _button, buttons;

	buttons[0, 0] = "Attack" // Type
	buttons[0, 1] = "s_button_attack" // Normal
	buttons[0, 2] = "s_button_attack_selected" // Selected
	buttons[0, 3] = "s_button_attack_disabled" // Disabled
	buttons[0, 4] = [CharacterState.ATTACK, CharacterState.QUICK_ATTACK, CharacterState.SPECIAL_ATTACK] // State to check
	buttons[0, 5] = 0 // Position
	
	buttons[1, 0] = "Defend"
	buttons[1, 1] = "s_button_defend"
	buttons[1, 2] = "s_button_defend_selected"
	buttons[1, 3] = "s_button_defend_disabled"
	buttons[1, 4] = [CharacterState.DEFEND, CharacterState.DODGE, CharacterState.SPECIAL_DEFEND]
	buttons[1, 5] = 1

	buttons[2, 0] = "Reload"
	buttons[2, 1] = "s_button_reload"
	buttons[2, 2] = "s_button_reload_selected"
	buttons[2, 3] = "s_button_reload_disabled"
	buttons[2, 4] = [CharacterState.RELOAD, CharacterState.CHARGE, CharacterState.SPECIAL]
	buttons[2, 5] = -1

	_button = instance_create_layer(0, 0, "Instances", o_button_parent);
	
	_button._button_type =  buttons[button_index][0];
	_button._player_id = character_instance_id;
	_button._button_position = buttons[button_index][5]
	_button._state_to_check = buttons[button_index][4];
	_button._default_sprite = asset_get_index(buttons[button_index][1]);
	_button._selected_sprite = asset_get_index(buttons[button_index][2]);
	_button._disabled_sprite = asset_get_index(buttons[button_index][3]);
	
	if (button_index == 0) {
		iglc_write(0, "Setting up button attack");
		_button._check_script = [character_instance_id._scr_attack_checker, character_instance_id._scr_quick_attack_checker]
	} else if (button_index == 1) {
		iglc_write(0, "Setting up button defend");
		_button._check_script = [character_instance_id._scr_defend_checker, character_instance_id._scr_dodge_checker]
	} else if (button_index == 2) {
		iglc_write(0, "Setting up button reload");
		_button._check_script = [character_instance_id._scr_reload_checker, character_instance_id._scr_charge_checker, character_instance_id._scr_special_checker];		
	}
}