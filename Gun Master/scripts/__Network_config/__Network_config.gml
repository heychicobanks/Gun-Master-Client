// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

//#macro SERVER_HOST "87.90.76.125"
#macro SERVER_HOST "localhost"
#macro SERVER_PORT 8888
#macro SERVER_TICK_RATE 60
#macro MAX_INPUT_BUFFER_SIZE 2

#macro CLIENT_TCP_PORT_MIN 5000
#macro CLIENT_UDP_PORT_MIN 7000

#macro CLIENT_TCP_PORT_MAX 6999
#macro CLIENT_UDP_PORT_MAX 9999