// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function manage_entities(parentId, entityId, type, components) {
    var entity;

    if (ds_map_exists(global.entityMap, entityId)) {
        entity = global.entityMap[? entityId];
    } else {
        entity = create_entity(parentId, entityId, type);
		
		if (!entity) return;
	    global.entityMap[? entityId] = entity;
    }
    // Mise à jour des composants de l'entité
	update_entity(entity, components);
}