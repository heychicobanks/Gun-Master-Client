// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function draw_collision_mask(object, _color, isDebugMode) {
	if (!isDebugMode)
		return;
	draw_set_alpha(0.25);
	draw_rectangle_color(x - _hitbox_width / 2, y - _hitbox_height / 2, x + _hitbox_width / 2,  y + _hitbox_height / 2, _color, _color, _color, _color, false);
	//	draw_rectangle_color(object.bbox_left,object.bbox_top,object.bbox_right,object.bbox_bottom,_color,_color,_color,_color,false);
	draw_set_alpha(1);
}