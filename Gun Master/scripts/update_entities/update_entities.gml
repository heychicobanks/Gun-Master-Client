// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function update_entity(entity, components) {
	if (variable_struct_exists(components, string(ComponentName.POSITION))) {
		show_debug_message("Position component");
		var positionData = components[$ string(ComponentName.POSITION)];
		handle_position_component(entity, positionData);
	}
	if (variable_struct_exists(components, string(ComponentName.SHOOT))) {
		show_debug_message("Shoot component");
	    var shootData = components[$ string(ComponentName.SHOOT)];
		handle_shoot_component(entity, shootData);
	}
	if (variable_struct_exists(components, string(ComponentName.DEFEND))) {
		show_debug_message("Defend component");
	    var defendData = components[$ string(ComponentName.DEFEND)];
		handle_defend_component(entity, defendData);
	}
	if (variable_struct_exists(components, string(ComponentName.HEALTH))) {
		show_debug_message("Health component");
	    var healthData = components[$ string(ComponentName.HEALTH)];
		handle_health_component(entity, healthData);
	}
	if (variable_struct_exists(components, string(ComponentName.VELOCITY))) {
		show_debug_message("Velocity component");
        var velocityData = components[$ string(ComponentName.VELOCITY)];
	}
	if (variable_struct_exists(components, string(ComponentName.DASH))) {
		show_debug_message("Dash component");
        var dashingData = components[$ string(ComponentName.DASH)];
		handle_dash_component(entity, dashingData);
	}
	if (variable_struct_exists(components, string(ComponentName.HITBOX))) {
		show_debug_message("Hitbox component");
        var hitboxData = components[$ string(ComponentName.HITBOX)];
		handle_hitbox_component(entity, hitboxData)
	}
	if (variable_struct_exists(components, string(ComponentName.GAME_CONTROLLER))) {
		//	show_debug_message("GameController component");
		var controllerData = components[$ string(ComponentName.GAME_CONTROLLER)];
		handle_game_controller_component(controllerData);
	}
}