// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function send_refreshToken(){
	if (global.refreshToken != "") // Si le refresh Token est présent on le renvoie
		send(PacketType.RefreshToken, {"refreshToken": global.refreshToken}); // NOTE : IMPORTANT <---------- Le mécanisme doit prendre en compte si le joueur est en jeu
	return;
}