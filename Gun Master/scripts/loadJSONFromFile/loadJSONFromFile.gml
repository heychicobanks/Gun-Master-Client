/// @description Load Characters from JSON

var _filename = argument0;
var json_str = "";
var _json;

// Vérifier si _filename est défini
if (is_string(_filename)) {

    // Vérifier si le fichier existe
    if (file_exists(_filename)) {

        var file = file_text_open_read(_filename);
        if (file != -1) {
            
            while (!file_text_eof(file)) {
                var line = file_text_readln(file);
                json_str += line;
            }
            _json = json_decode(json_str)
			if (_json == -1) {
				show_debug_message("Erreur lors du traitement du fichier " + _filename)
				return undefined
			}
            file_text_close(file);

        } else {
            show_debug_message("Erreur : Impossible d'ouvrir le fichier " + _filename);
            return undefined;
        }

    } else {
        show_debug_message("Erreur : Le fichier " + _filename + " n'a pas été trouvé.");
        return undefined;
    }

} else {
    show_debug_message("Erreur : Le nom du fichier est indéfini ou invalide.");
    return undefined;
}

if (ds_map_exists(_json, "default"))
    return (_json[? "default"])
return (_json);