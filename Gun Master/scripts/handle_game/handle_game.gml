// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_game(packet){
	if (packet.status.code != OK) {
		show_debug_message("Error for game packet : " + packet.status.message);
		return;
	}
	show_debug_message("Game packet : " + json_stringify(packet.payload));
	switch (packet.payload.state) {
		case "INIT": {
//			global.game_socket.ip = packet.payload.ip;
			global.game_socket.ip = SERVER_HOST;
			global.game_socket.port = packet.payload.port;
			
			global.game_socket.socket = network_create_socket_ext(network_socket_udp, global.client.udp_port);
			
			send(PacketType.Game, {matchId: global.game.matchId, port: global.client.udp_port});
			break;
		}
		case "PAUSE": {
			show_debug_message("Game is paused, one or more players are disconnected");
			break;
		}
		case "ACTIVE": {
			room_goto(r_remote_arena_default);
			break;
		}
		default:
			show_debug_message("Invalid game state : " + string(packet.payload.state))
			break;
	}
	return;
}