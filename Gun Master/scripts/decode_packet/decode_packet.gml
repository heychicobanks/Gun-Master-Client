// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function decode_packet(buffer) {
	if (buffer_exists(global.client.buffer)) { // Un buffer incomplet existe déjà
			var new_buff = buffer_create(1, buffer_grow, 1);
			
			// On ajoute les deux buffers dans le nouveau buffer afin de les combiner
			buffer_copy(global.client.buffer, 0, buffer_get_size(global.client.buffer), new_buff, 0);
			buffer_copy(buffer, 0, buffer_get_size(buffer), new_buff, buffer_get_size(new_buff));
			
			// On supprime les buffers obsoletes
			buffer_delete(buffer);
			buffer_delete(global.client.buffer);

			buffer = new_buff;
			
			// On réinitialise le buffer tampon
			global.client.buffer = -1;			
	}
	
	var buffer_size = buffer_get_size(buffer);
	
	var i = 0;
	
	while (i < buffer_size) { // Tant qu'on est pas arrivé à la fin du buffer
		var packSize = buffer_peek(buffer, i, buffer_u16); // this also seeks

		if (i + packSize > buffer_size) { // Pas assez de données d'après la taille indiqué, le buffer est partiel
			global.client.buffer = buffer_create(1, buffer_grow, 1);
			buffer_copy(buffer, i, i + 2 + packSize, global.client.buffer, 0);
			break; // On quitte la boucle
		}
		
		i += 2; // On saute les 2 octets de la taille
		
		var data = SnapBufferReadMessagePack(buffer, i);
		handle_packet(data);
        
		i += packSize;
	}
	buffer_delete(buffer);
}
