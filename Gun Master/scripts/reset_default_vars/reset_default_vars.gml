// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function reset_default_vars(){
	_defend = 0;
	_hasExecuted = false;
	_state = CharacterState.IDLE;
	image_xscale = 1 * _facing_direction;
	image_yscale = 1;
}