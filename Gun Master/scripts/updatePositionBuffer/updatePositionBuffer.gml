// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function updatePositionBuffer(position_buffer, x, y) {
    if (ds_queue_size(position_buffer) >= MAX_INPUT_BUFFER_SIZE)
        ds_queue_dequeue(position_buffer);
    ds_queue_enqueue(position_buffer, {"x": x, "y": y});
}