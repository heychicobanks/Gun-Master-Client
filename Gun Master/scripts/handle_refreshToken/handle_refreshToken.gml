// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_refreshToken(packet){
	// Le token n'a pas pu être mis à jour
	if (packet.status.code != OK) {
		show_debug_message("RefreshToken failed");
	
		global.token = "";
		global.refreshToken = ""; // <--------- Le fichier refreshToken devra être supprimé
		return;
	}
	
	show_debug_message("RefreshToken successfull");

	// On met à jour le token
	global.token = packet.payload.token;
}