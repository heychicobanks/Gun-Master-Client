// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_hitbox_component(entity, hitboxData){
	//show_debug_message(json_stringify(hitboxData))
	entity._hitbox_x0= hitboxData.x0;
	entity._hitbox_x1 = hitboxData.x1;
	entity._hitbox_y0= hitboxData.y0;
	entity._hitbox_y1 = hitboxData.y1;
}