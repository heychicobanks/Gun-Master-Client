// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_reload_sheriff(character, isNetwork, isRemote){
	show_debug_message("Reload attack sheriff. IsNetwork = " + string(isNetwork) + ", isRemote = " + string(isRemote));
	if (!character._scr_reload_checker(character)) {
		character._state = CharacterState.IDLE;
		return;
	}
	character._ammo_slots[character._current_ammo_slot] += 1;
	sprite_index = character._s_reload;

	var random_index = irandom(ds_list_size(character._sfx_reload) - 1);
	audio_play_sound(asset_get_index(character._sfx_reload[| random_index]), 1, false);
}