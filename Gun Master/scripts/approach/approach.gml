// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function approach(argument0, argument1, argument2) {
	var _current;
	var _target;
	var _amount;

	_current = argument0;
	_target = argument1;
	_amount = argument2;

	if (_current < _target)
	    return min(_current + _amount, _target)
	else
	    return max(_current - _amount, _target)
}

