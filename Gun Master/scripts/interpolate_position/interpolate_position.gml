// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function interpolate_position(){
	if (!is_correcting)
		return;
	
	if (x == current_x && y == current_y) {
		show_debug_message("Position fixed : x = " + string(x) + ", y = " + string(y) + " (, " + string(current_x) + "," + string(current_y) + ")");
		is_correcting = false;
	}

	
	var alpha = (current_time - last_update) / SERVER_TICK_RATE;
	show_debug_message("alpha = " + string(alpha));

	show_debug_message("Lerping position : x = " + string(x) + ", y = " + string(y) + " (, " + string(lerp(x, current_x, clamp(alpha, 0, 1))) + "," + string(lerp(y, current_y, clamp(alpha, 0, 1))) + ")");
	x = lerp(x, current_x, clamp(alpha, 0, 1));
	y = lerp(y, current_y, clamp(alpha, 0, 1));
	show_debug_message("real x = " + string(x));
	show_debug_message("current_x = " + string(current_x));
	show_debug_message("real y = " + string(y));
	show_debug_message("current_y = " + string(current_y));
}