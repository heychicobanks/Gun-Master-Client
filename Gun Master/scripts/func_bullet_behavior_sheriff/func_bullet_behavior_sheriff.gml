// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_bullet_behavior_sheriff(){
	direction += -0.5 + random(1) 
	speed =  _speed * room_speed * global.dt_steady * _direction;
	show_debug_message("speed = " + string(speed))
	add_trail_sheriff(x,y,x_origin, y_origin, depth, image_blend);
}