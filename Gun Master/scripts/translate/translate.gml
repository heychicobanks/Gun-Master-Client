// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function translate(key){
	var text = "";
	
	if (global.translation[? key] != undefined) {
		var text = global.translation[? key];
		
		var a = argument_count > 1 ? argument[1] : "";
		text = string_replace_all(text, "{0}", a)
	} else {
		var text = key;
	}
	return (text);
}