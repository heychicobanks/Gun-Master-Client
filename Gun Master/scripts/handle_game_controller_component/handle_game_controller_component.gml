// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_game_controller_component(data){	
	date_set_timezone(timezone_utc);
	
	var serverTimestamp = floor(data.startTime / 1000);
	var now_date = date_second_span(date_create_datetime(1980,1,1,0,0,0), date_current_datetime()) + 315532800; // Normalement pour utiliser EPOCH de Linux c'est 1/1/1970 sauf que la création de datge gamemaker plante selon la platefgorme
	var latency = now_date - serverTimestamp;	
	var goal = data.seconds;

	var result = goal - latency;
	result = clamp(result, 0, goal);
	
	global.countdown =  result > 0 ? result : 0;
	global.roundState = data.current_step;
	global.scale = data.scale;
	
	date_set_timezone(timezone_local);
	
	/*
	show_debug_message("New round state : " + string(global.roundState));
	show_debug_message("new gamestep packet : " + json_stringify(data));
	show_debug_message("server timeStamp = " + string(serverTimestamp));
	show_debug_message("now timeStamp = " + string(now_date));
	show_debug_message("latency = " + string(latency));
	show_debug_message("result = " + string(result));
	*/
}