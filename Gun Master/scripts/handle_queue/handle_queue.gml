// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_queue(packet){
	if (packet.status.code != OK) {
		show_debug_message("Error while queuing : " + packet.status.message);
		return;
	}
	
	show_debug_message("queuing successfull");
		
	global.client.state = ClientState.INQUEUE;
}