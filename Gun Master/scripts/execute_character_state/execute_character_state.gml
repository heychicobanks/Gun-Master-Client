// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function execute_character_state(character, new_state){
	character._chosen_state = new_state;
	character._hasExecuted = false;
}