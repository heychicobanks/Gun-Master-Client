// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

enum PacketType {
    Register,
    Login,  
    Logout,
    Ping,
    Pong,
    Error,
    Achievement,
    Notification,
    RefreshToken,
    Queue,
    Dequeue,
    Handshake,
    FriendRequest,
    Input,
    Matchmaking,
    Profile,
    Game,
    Entities,
}

enum MatchMakingAction {
    QUEUE,
    DEQUEUE,
}

enum FriendRequestAction {
    SEND,
    ACCEPT,
    DECLINE,
    REMOVE,
    BLOCK,
}

enum ClientState {
    HANDSHAKE,
    IDLE,
    INQUEUE,
    INLOBBY,
    INGAME,
}

enum GameState {
	START,
	RESUME,
	PAUSED,
	END
}

enum RoundState {
    INIT,
    PREPARATION,
    ACTION,
    ROUND_END,
    GAME_END,
    PAUSED
}

enum MatchState {
    INIT,
    PAUSED,
    ACTIVE,
    INACTIVE,
    FINISHED,
}

enum MovementSource {
	MOVE,
	DASH,
	COLLISION
}

enum Direction {
	LEFT = -1,
	RIGHT = 1
}

enum POSITION {
	RIGHT = 1,
	LEFT = 0
}

enum FACING {
	RIGHT = 1,
	LEFT = -1
}

enum InputType {
    DEFEND,
    SPECIAL_DEFEND,
    BASIC_ATTACK,
    QUICK_ATTACK,
    SPECIAL_ATTACK,
    DODGE,
    RELOAD,
    SPECIAL_RELOAD,
    MOVE_LEFT,
    MOVE_RIGHT,
    EMOJI
}

enum CharacterState {
		IDLE,
		DEAD,
		MOVE,
		RELOAD,
		ATTACK,
		DEFEND,
		DODGE,
		CHARGE,
		SPECIAL,
		QUICK_ATTACK,
		SPECIAL_DEFEND,
		SPECIAL_ATTACK,
		COL_COUNT
}

enum TIMER_MODE {
	REGULAR,
	IRREGULAR,
};

enum TIMER_DISPLAY {
	INVISIBLE,
	STANDARD,
	SUSPENSE
};

#macro UNRANKED "UNRANKED"
#macro BRONZE "BRONZE"
#macro SILVER "SILVER"
#macro GOLD "GOLD"
#macro PLATINUM "PLATINUM"
#macro DIAMOND "DIAMOND"
#macro GUNMASTER "GUN MASTER"

#macro OK 200
#macro BAD_REQUEST 400
#macro UNAUTHORIZED 401
#macro FORBIDDEN 403
#macro NOT_FOUND 404
#macro TIME_OUT 408
#macro TOO_MANY_REQUESTS 429
#macro INTERNAL_SERVER_ERROR 500