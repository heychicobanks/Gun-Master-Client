// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function create_character(character_id, character_x, character_y, character_position, character_type, isNetwork = false){
	var entity;
	
	if (isNetwork) {
		if (character_type == CharacterType.PLAYER) {
			entity = instance_create_depth(-100, -100, 0, o_player_network);
		} else if (character_type == CharacterType.REMOTE) {
			entity = instance_create_depth(-100, -100, 0, o_player_remote)
		} else {
			show_debug_message("Invalid character type");
			return			
		}
	} else {
		if (character_type == CharacterType.PLAYER) {
			entity = instance_create_depth(-100, -100, 0, o_player);
		} else if (character_type == CharacterType.BOT) {
			entity = instance_create_depth(-100, -100, 0, o_bot);
		} else {
			show_debug_message("Invalid character type");
			return
		}
	}
	
	// On initialise le personnage
	entity.initialize(character_id, character_position, character_type == CharacterType.PLAYER);
	
	return entity;
}