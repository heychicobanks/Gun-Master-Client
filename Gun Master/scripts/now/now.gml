// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function now(){
	date_set_timezone(timezone_utc);
	var today_date = date_second_span(date_create_datetime(1980,1,1,0,0,0), date_current_datetime()) + 315532800;
	date_set_timezone(timezone_local);
	return today_date
}