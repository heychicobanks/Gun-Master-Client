// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_move_network(_character, _direction){
	if (!func_move_check(_character, _direction))
		return;
		
	if (_character.is_correcting)
		_character.current_x = _character.current_x + (_character._step_distance * _direction); 
	else
		_character.x = _character.x + (_character._step_distance * _direction);
	
	var emit_x = _character.x - (sprite_get_width(_character.sprite_index) / 2) * _direction;
    var emit_y = _character.y + sprite_get_height(_character.sprite_index) / 2;
	part_type_scale(partType, 1 * _direction, 1);
	audio_play_sound(sfx_dash_move, 1, false);
	part_particles_create(partSystem, emit_x, emit_y, partType, 1);
	
	send(PacketType.Input, { "input": _direction == Direction.RIGHT ? InputType.MOVE_RIGHT : InputType.MOVE_LEFT});
	updatePositionBuffer(_character.position_buffer, _character.is_correcting ? _character.current_x : _character.x, _character.y);
}