// Pubsub Manager Script File

function pubsub_subscribe(_event, _func) {
    with (c_pubsub) {
        subscribe(other.id, _event, _func);
        return true;
    }
    return false;
}

function pubsub_unsubscribe(_event) {
    with (c_pubsub) {
        unsubscribe(other.id, _event);
        return true;
    }
    return false;
}

function pubsub_unsubscribe_all() {
	with (c_pubsub) {
        unsubscribe_all(other.id);
        return true;
    }
    return false;
}

function pubsub_publish(_event, _data) {
    with (c_pubsub) {
        publish(_event, _data);
        return true;
    }
    return false;
}