// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_velocity_component(entity, velocityData){
	show_debug_message("hspeed : " + string(velocityData.hspeed) + " vspeed : " + string(velocityData.vspeed));
    entity.hspeed = velocityData.hspeed;
    entity.vspeed = velocityData.vspeed;
}