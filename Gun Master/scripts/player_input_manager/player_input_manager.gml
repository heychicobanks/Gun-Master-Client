// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function player_input_manager(){
	if keyboard_check_pressed(vk_left) {
		if (global.roundState == RoundState.PREPARATION) {
			_chosen_state = CharacterState.RELOAD
			send_input(InputType.RELOAD);
		}
			/*if (global.roundState == RoundState.ACTION) {
				if (_cached_state = CharacterState.SPECIAL) {
					execute_character_state(self.id, CharacterState.CHARGE)
				} else {
					execute_character_state(self.id, CharacterState.SPECIAL)
					_cached_state = CharacterState.SPECIAL;
					alarm[1] = _combo_delay_time;
				}
			}*/
		audio_play_sound(sfx_button_click_1, 1, false);
	}

	if keyboard_check_pressed(vk_right) {
		if (global.roundState == RoundState.PREPARATION) {
			_chosen_state = CharacterState.DEFEND;
			_hasExecuted = false;
			send_input(InputType.DEFEND);
		}
			if (global.roundState == RoundState.ACTION) {
				if (_state = CharacterState.CHARGE) {
					send_input(InputType.SPECIAL_DEFEND);
					_chosen_state = CharacterState.SPECIAL_DEFEND;
					_hasExecuted = false;
				} else {
					send_input(InputType.DODGE);
					_chosen_state = CharacterState.DODGE
					_hasExecuted = false;
				}
			}
		audio_play_sound(sfx_button_click_1, 1, false);
	}
	
	if keyboard_check_pressed(vk_down) {
		if (global.roundState == RoundState.PREPARATION) {
			_chosen_state = CharacterState.ATTACK
			send_input(InputType.BASIC_ATTACK);
		}
			if (global.roundState == RoundState.ACTION) {
				if (_state = CharacterState.CHARGE) {
					send_input(InputType.SPECIAL_ATTACK);
					_chosen_state = CharacterState.SPECIAL_ATTACK;
					_hasExecuted = false;
				} else {
				send_input(InputType.QUICK_ATTACK);
				_chosen_state = CharacterState.QUICK_ATTACK
				_hasExecuted = false
				}
			}
		audio_play_sound(sfx_button_click_1, 1, false);
	}
	
	if keyboard_check_pressed(ord("A")) {
		//_scr_reload(id);
		send_input(InputType.RELOAD);
	}
	
	if keyboard_check_pressed(ord("B")) {
		uc_bars();
	}
	
	/*
	if keyboard_check_pressed(ord("C")) {
		_xscale = 2;
		_yscale = 2;
	}*/
	
	if keyboard_check_pressed(ord("E")) {
		if (global.timer_display == TIMER_DISPLAY.INVISIBLE) {
			global.timer_display = TIMER_DISPLAY.STANDARD;
			iglc_write(0, "Timer display set on standard");
		} else if (global.timer_display == TIMER_DISPLAY.STANDARD) {
			global.timer_display = TIMER_DISPLAY.SUSPENSE;
			iglc_write(0, "Timer mode set on suspense");
		} else {
			global.timer_display = TIMER_DISPLAY.INVISIBLE;
			iglc_write(0, "Timer mode set on INVISIBLE");
		}
	}
	
	if keyboard_check_pressed(ord("Q")) {
		/*
		if (_state == CharacterState.DEAD)
			exit;
//		stretch(self.id, 1.4, 1);
		func_move_network(self.id, Direction.LEFT);*/
	}
	
	if keyboard_check_pressed(ord("R")) {
		game_restart();
	}
		
	if keyboard_check_pressed(ord("S")) {
		/*
		if (_state == CharacterState.DEAD)
			exit;
//		stretch(self.id, 1.4, 1);
		func_move_network(self.id, Direction.RIGHT);*/
}
	
	if keyboard_check_pressed(ord("T")) {
		with (c_timer) {
			_isBlocked = !_isBlocked;
			iglc_write(0, "Timer is blocked !");
		}
	}
	
	if keyboard_check_pressed(ord("Z")) {
		if (global.timer_mode == TIMER_MODE.REGULAR) {
			global.timer_mode = TIMER_MODE.IRREGULAR;
			iglc_write(0, "Timer mode set on irregular");
		} else {
			global.timer_mode = TIMER_MODE.REGULAR;
			iglc_write(0, "Timer mode set on regular");
		}
	}
	
}