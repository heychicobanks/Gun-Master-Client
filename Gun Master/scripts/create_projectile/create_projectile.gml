// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function create_projectile(character, isNetwork = false){
		// Attention: peut potentiellement ne pas être synchronisé avec la hitbox/origine du serveur
		// ATTENTION: POSITION 
		var projectile_origin_x = character._position == POSITION.LEFT ? character.x + (-16 + character._s_attack_origin_x) : character.x + (-16 + character._s_attack_origin_x) * -1;
		var projectile_origin_y = character.y - character.sprite_height/ 2 + character._s_attack_origin_y;
		
		if (isNetwork)
			var instance = instance_create_layer(projectile_origin_x, projectile_origin_y, "Instances", o_projectile_parent_network);
		else
			var instance = instance_create_layer(projectile_origin_x, projectile_origin_y, "Instances", o_projectile_parent);			

		instance.initialize(character._projectile_id, character.id, character._state, character._facing_direction);
}