// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function create_handshake_payload(){
	    var os_name;

    switch (os_type) {
        case os_windows:
            os_name = "Windows";
            break;
        case os_macosx:
            os_name = "macOS";
            break;
        case os_linux:
            os_name = "Linux";
            break;
        case os_android:
            os_name = "Android";
            break;
        case os_ios:
            os_name = "iOS";
            break;
        default:
            os_name = "Unknown";
    }
	return {
		"clientVersion": "1.0.0", // Récupérer la version
		"device": os_name,
		"language":	os_get_language()
	}
}