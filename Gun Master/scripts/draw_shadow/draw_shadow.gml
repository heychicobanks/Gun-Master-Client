/// @description  draw_shadow(x, y, rad)
/// @param x
/// @param  y
/// @param  rad
function draw_shadow(argument0, argument1, argument2, argument3) {
	var _x = argument0;
	var _y = argument1;
	var rx = argument2;
	var ry = argument3 / 2;
	draw_set_color(c_black);
	draw_set_alpha(0.023);
	draw_ellipse(_x - rx, _y - ry, _x + rx - 3, _y + ry, false);
	draw_set_alpha(1);
}
