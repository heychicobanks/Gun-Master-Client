// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function onEventReceived(eventType){
	switch(eventType) {
		case ("ATTACK"):
			_state = CharacterState.ATTACK;
			break;
		case ("DEFEND"):
			_state = CharacterState.DEFEND;
			break;
		case ("RELOAD"):
			_state = CharacterState.RELOAD;
			break;
		case ("IDLE"):
		default:
			_state = CharacterState.IDLE;
			break;
	}
}