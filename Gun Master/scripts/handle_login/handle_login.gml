// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function handle_login(packet){
	if (packet.status.code != OK) {
		show_debug_message("Error while logging in : " + packet.status.message);
		with (o_button_login) {
			active = true; // Le bouton est de nouveau disponible
		}
		with (o_input_username) {
			active = true;
		}
		with (o_input_password) {
			active = true;
		}
		return;
	}
	
	show_debug_message("Login succesfull");
	// On stocke les informations essentielles au client
	
	// Session
	global.token = packet.payload.token;
	global.refreshToken = packet.payload.refreshToken;
	global.client.is_authenticated = true
	// On charge les données avant de passer à la room principale
	send(PacketType.Profile, {});	
	
	room_goto(r_main);
}