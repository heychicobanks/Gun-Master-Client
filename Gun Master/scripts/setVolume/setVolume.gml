// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function setVolume(_key, _value) {
    soundSettings[? _key] = _value;
    var _settings_str = json_encode(soundSettings);
    file_text_write_string("sound_settings.json", _settings_str);
}