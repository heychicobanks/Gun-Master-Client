// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_shoot_component(entity, shootData){
	show_debug_message("ammo : " + string(shootData.ammo) + " max_ammo : " + string(shootData.max_ammo) + " shootOuput : " + json_stringify(shootData.shoot_output));
	
	if (entity.object_index == o_player_remote) {
		show_debug_message("player remote");
		
		// Itérer sur shoot_output
		var size = array_length(shootData.shoot_output)
		for (var i = 0; i < size; i++) {
			var action = shootData.shoot_output[i];
			
			switch (action) {
				case "reload": {
					entity._chosen_state = CharacterState.RELOAD;
					entity._hasExecuted = false;
					break;
				}
				case "basic_attack": {
					entity._chosen_state = CharacterState.ATTACK;
					entity._hasExecuted = false;
					break;
				}
				case "quick_attack": {
					entity._chosen_state = CharacterState.QUICK_ATTACK;
					entity._hasExecuted = false;
					break;
				}
				case "special_special": {
					entity._chosen_state = CharacterState.SPECIAL_ATTACK;
					entity._hasExecuted = false;
					break;
				}
				default: {
					show_debug_message("Unknow shooting action : " + string(action));
					break
				}
			}
		}
		entity._max_ammo = shootData.max_ammo;
	}
	
	if (entity.object_index == o_player_network) {
		show_debug_message("player network");
	}
	
	//entity.last_update = current_time;
}	