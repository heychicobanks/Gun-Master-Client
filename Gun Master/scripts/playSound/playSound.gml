// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function PlaySound(_sound, _volume) {
    audio_sound_gain(_sound, _volume, 0);
    audio_play_sound(_sound, 1, false);
}
