// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function send(type, payload, connectionType = ConnectionType.TCP){
	var paquet = create_packet(type, payload);
	if (connectionType == 0) 
		global.base_socket.state = send_packet(paquet, global.base_socket.socket)
	else
		global.game_socket.state = send_packet(paquet, global.game_socket.socket, ConnectionType.UDP)
}