// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function init_character_particles(_character){
	partSystem = part_system_create();

	partType = part_type_create();
	part_type_sprite(partType, s_dash_wind, true, false, false);
	part_type_size(partType, 1, 1, 0, 0);
	part_type_speed(partType, 0.1, 0.2, 0, 0);
	part_type_direction(partType, 180, 0, 0, 0);
	part_type_life(partType, 10, 10);
	part_type_orientation(partType, 0, 0, 0, 0, false);
}