// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function standard_bot_behavior(_difficulty, _behavior){
	switch (global.roundState) {
		case (RoundState.PREPARATION):
			if (has_chosen_prep)
				break;
			has_chosen_prep = true;
			has_chosen_fight = false;
			switch (_behavior) {
				case (BEHAVIOR.PEACEFUL):
					_chosen_state = CharacterState.IDLE;
					break;
				case (BEHAVIOR.NOOB):
					_chosen_state = irandom_range(0, CharacterState.COL_COUNT);
					break;
				case (BEHAVIOR.BALANCED):
					var possible_states = [];

					if (_scr_reload_checker(self.id))
					    array_push(possible_states, CharacterState.RELOAD);
					if (_scr_defend_checker(self.id))
					    array_push(possible_states, CharacterState.DEFEND);
					if (_scr_attack_checker(self.id))
						array_push(possible_states, CharacterState.ATTACK);

					var random_index = irandom(array_length(possible_states) - 1);
					_chosen_state = possible_states[random_index];
					iglc_write(0, "AI Chosen : " + string(_chosen_state));
					break;
			}
			break;
		case (RoundState.ACTION):
			if (has_chosen_fight)
				break
			has_chosen_prep = false;
			if (instance_exists(o_projectile_parent)) {
				if (o_projectile_parent._source_id != self.id) {
					if (distance_to_object(o_projectile_parent) < 200)
						if (irandom(5) >= 100) {
							_chosen_state = CharacterState.DODGE;
							_hasExecuted = false
							has_chosen_fight = true;
						}
				}
			}
			break;
	}
}