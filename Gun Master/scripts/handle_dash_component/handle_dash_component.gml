// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_dash_component(entity, dashingData){
	show_debug_message(json_stringify(dashingData));
	//show_debug_message("dashing output : " + json_stringify(dashingData.dash_output));
	
	var size = array_length(dashingData.dash_output);
	
	// Si l'entité est le player pas besoin de rejouer le dash
	if (entity.object_index == o_player_network) return;
	
	for (var i = 0; i < size; ++i) {
		var dash = dashingData.dash_output[i];
		
		if (entity.object_index == o_player_remote) {
			var emit_x = entity.x - (sprite_get_width(entity.sprite_index) / 2) * dash;
			var emit_y = entity.y + (sprite_get_height(entity.sprite_index) / 2);
			
			//On joue le dash
			part_type_scale(entity.partType, 1 * dash, 1);
			audio_play_sound(sfx_dash_move, 1, false);
			part_particles_create(entity.partSystem, emit_x, emit_y, entity.partType, 1);
		}
	}
}