// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function init_default_vars(){
	_hp = _max_hp
	_ammo = 0;
	_defend = 0;
	_steps = 0;
 	
	_counter_number = 0;
	_counter = false;
	
	_chosen_state = CharacterState.IDLE;
	_state = _chosen_state;
	
	_hasExecuted = false; // Afin d'executer l'action choisie qu'une fois

	_depth_factor = 0.1 // Effet de profondeur lors des actions (xscale, yscale)
	
	_xscale = 1;
	_yscale = 1;
	_is_scale_animating = false;

	image_speed = 0;
	sprite_index = _s_idle
}