// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function get_deltatime(){
	dt_previous = dt_current;
	dt_current = delta_time / 1000000;
	
	if (dt_current > 1 / minFPS) {
		if (dtRestored) { 
			dt_current = 1/ minFPS; 
		} else { 
        dt_current = dt_previous;
        dtRestored = true;
		}
	} else {
		dtRestored = false;
	}

	global.dt_steady = dt_current * global.dt_scale;
}