// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function init_character_base_components(){
	
	// Position
	server_x = x;
	server_y = y;

	// Hitbox
	_hitbox_x0 = 0;
	_hitbox_x1 = 0;
	_hitbox_y0 = 0;
	_hitbox_y1 = 0;
	
	last_update = current_time; // Utilisé pour la prédiction/réconciliation
		
	is_correcting = false; // Permet d'utiliser la réconciliation
	
	// HP
	_hp = 0;
	_max_hp = 0;
	
	// Shoot
	_max_ammo = 0;
	_ammo = 0;


	// Defend
	_max_defend = 0;
	_defend = 0;
}