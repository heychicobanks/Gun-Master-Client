// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function apply_easing(_instance, _target, _type, _speed){
	_instance._xscale = easing(_instance._xscale, _target, _type, _speed);
	_instance._yscale = easing(_instance._yscale, _target, _type, _speed);
}