// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function handle_packet(packet) {		
		if (packet.status.code == UNAUTHORIZED) // On envoie le refreshToken
			handle_unauthorized();
		switch (packet.packetType) {
			case PacketType.Handshake:
				handle_handshake(packet);
				break;
			case PacketType.Ping:
				handle_ping(packet);
				break;
			case PacketType.Pong:
				handle_pong(packet);
				break;
			case PacketType.Login:
				handle_login(packet);
				break;
			case PacketType.Logout:
				handle_logout(packet);
				break;
			case PacketType.RefreshToken:
				handle_refreshToken(packet);
				break;
			case PacketType.Queue:
				handle_queue(packet);
				break;
			case PacketType.Dequeue:
				handle_dequeue(packet);
				break;
			case PacketType.Entities:
				handle_entities(packet);
				break;
			case PacketType.Game:
				handle_game(packet);
				break;
			case PacketType.Error:
				handle_error(packet);
				break;
			case PacketType.Profile:
				handle_profile(packet);
				break;
			case PacketType.Matchmaking:
				handle_matchmaking(packet);
				break;
			default:
				show_debug_message("Unknown packet type " + string(packet.packetType));
				break;
		}
}
