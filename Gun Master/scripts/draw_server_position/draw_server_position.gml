// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function draw_server_position(object,  isDebugMode = false, _color = c_green) {
	if (!isDebugMode)
		return;
	draw_set_alpha(0.25);
	draw_rectangle_color(object._hitbox_x0, object._hitbox_y0, object._hitbox_x1, object._hitbox_y1,_color,_color,_color,_color,false);
	draw_set_alpha(0.35);
	draw_sprite_ext(sprite_index, image_index, object.server_x, object.server_y, image_xscale, image_yscale, image_angle, c_red, 0.55);
	//	draw_rectangle_color(object.bbox_left,object.bbox_top,object.bbox_right,object.bbox_bottom,_color,_color,_color,_color,false);
	draw_set_alpha(1);
}