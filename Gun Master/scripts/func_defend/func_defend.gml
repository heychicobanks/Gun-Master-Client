// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations

function func_defend(character){
	if (!character._scr_defend_checker(character)) {
		character._state = CharacterState.IDLE;
		return;
	}
	character._defend += 1;
	character.sprite_index = character._s_defend;
}