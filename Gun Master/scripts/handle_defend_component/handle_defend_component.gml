// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_defend_component(entity, defendData){
	show_debug_message(json_stringify(defendData));
	
	if (entity.object_index == o_player_remote) {
		switch (defendData.state) {
			case "dodge":
				entity._chosen_state = CharacterState.DODGE;
				entity._hasExecuted = false;
				break;
			case "defend":
				entity._chosen_state = CharacterState.DEFEND;
				entity._hasExecuted = false;
				break;
			default:
				break;
		}
	} else {
	}
}