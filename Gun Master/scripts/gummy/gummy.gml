/// gummy(scales, target_x, target_y, duration, speed)
function gummy(scales, target_x, target_y, duration, speed) {
    static _effect_time = 0;

    var progress = clamp(_effect_time / duration, 0, 1);

    var new_x = approach(scales[0], lerp(scales[0], target_x, progress), speed);
    var new_y = approach(scales[1], lerp(scales[1], target_y, progress), speed);

    scales[@ 0] = new_x;
    scales[@ 1] = new_y;

    if (_effect_time >= duration) {
        _effect_time = 0;
        return false; // Indique que l'effet est terminé
    } else {
        _effect_time++;
        return true; // Indique que l'effet est toujours actif
    }
}
