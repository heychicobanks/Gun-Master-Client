// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_special_sheriff(character){
	_current_ammo_slot = (_current_ammo_slot == 1)? 0 : 1;
	character.sprite_index = character._s_special;
}