// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function handle_matchmaking(packet){
	if (packet.status.code != OK) {
		show_debug_message("Error during matchmaking : " + packet.status.code == TIME_OUT ? "Client didn't respond on time" : packet.status.message);

		// On reset les données du match
		global.game.opponent_character_id = undefined;
		global.game.client_id = undefined;
		global.game.position = undefined;
		global.game.opponent_name = undefined;
		global.game.character_id = undefined;
		global.game.skin_id = undefined;
		global.game.level = undefined;
		global.client.position = undefined;
		return;
	}
	
	show_debug_message("Match found ! : " + json_stringify(packet.payload));
	
	// On initialise les données du match
	var payload = packet.payload;

	global.game.opponent_character_id = payload.character_id;
	global.game.client_id = payload.client_id;
	global.game.position = payload.position;
	global.game.self_position = payload.self_position;
	global.game.opponent_name = payload.name;
	global.game.character_id = payload.character_id;
	global.game.skin_id = payload.skin_id;
	global.game.level = payload.level;
	global.game.matchId = payload.matchId;
	
	// On envoie automatiquement la réponse au serveur
	send(PacketType.Matchmaking, { id: packet.payload.matchId, accept: true});
}