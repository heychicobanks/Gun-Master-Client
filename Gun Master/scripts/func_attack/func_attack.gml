// Les actifs du script ont changé pour v2.3.0 Voir
// https://help.yoyogames.com/hc/en-us/articles/360005277377 pour plus d’informations
function func_attack(character){
	if (!character._scr_attack_checker(character)) {
		character._state = CharacterState.IDLE;
		return;
	}
	character._ammo -= 1;
	character.sprite_index = character._s_attack;

	var random_index = irandom(ds_list_size(character._sfx_attack_normal) - 1);
	audio_play_sound(asset_get_index(character._sfx_attack_normal[| random_index]), 1, false);
}