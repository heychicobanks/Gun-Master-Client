{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_hit_sheriff_1",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 0.100045,
  "parent": {
    "name": "Impact",
    "path": "folders/Sons/Characters/Sheriff/Attack/Impact.yy",
  },
  "preload": false,
  "sampleRate": 48000,
  "soundFile": "sfx_hit_sheriff_1.wav",
  "type": 0,
  "volume": 1.0,
}