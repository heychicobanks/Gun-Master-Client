{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_normal_sheriff_5",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 1.822472,
  "parent": {
    "name": "Normal",
    "path": "folders/Sons/Characters/Sheriff/Attack/Normal.yy",
  },
  "preload": false,
  "sampleRate": 48000,
  "soundFile": "sfx_normal_sheriff_5.wav",
  "type": 1,
  "volume": 1.0,
}