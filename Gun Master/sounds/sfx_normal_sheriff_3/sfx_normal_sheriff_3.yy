{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_normal_sheriff_3",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 1.858685,
  "parent": {
    "name": "Normal",
    "path": "folders/Sons/Characters/Sheriff/Attack/Normal.yy",
  },
  "preload": false,
  "sampleRate": 48000,
  "soundFile": "sfx_normal_sheriff_3.wav",
  "type": 0,
  "volume": 1.0,
}