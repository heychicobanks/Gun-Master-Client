{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_dash_move",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 0,
  "conversionMode": 0,
  "duration": 0.5072,
  "parent": {
    "name": "Move",
    "path": "folders/Sons/Characters/Misc/Move.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "sfx_dash_move.mp3",
  "type": 0,
  "volume": 1.0,
}