{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_reload_sheriff",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 0.0,
  "parent": {
    "name": "Reload",
    "path": "folders/Sons/Characters/Sheriff/Reload.yy",
  },
  "preload": false,
  "sampleRate": 48000,
  "soundFile": "sfx_reload_sheriff.wav",
  "type": 0,
  "volume": 1.0,
}