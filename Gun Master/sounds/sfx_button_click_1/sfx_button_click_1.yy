{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_button_click_1",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 0,
  "conversionMode": 0,
  "duration": 0.136803,
  "parent": {
    "name": "Buttons",
    "path": "folders/Sons/Arenas/Misc/Buttons.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "sfx_button_click_1.wav",
  "type": 0,
  "volume": 1.0,
}