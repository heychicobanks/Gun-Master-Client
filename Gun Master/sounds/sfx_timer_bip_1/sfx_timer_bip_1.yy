{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_timer_bip_1",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 0.04475,
  "parent": {
    "name": "Timer",
    "path": "folders/Sons/Arenas/Misc/Timer.yy",
  },
  "preload": false,
  "sampleRate": 48000,
  "soundFile": "sfx_timer_bip_1.wav",
  "type": 0,
  "volume": 1.0,
}