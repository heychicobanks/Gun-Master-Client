{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_shell",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 0,
  "conversionMode": 0,
  "duration": 2.142857,
  "parent": {
    "name": "shell",
    "path": "folders/Sons/Effects/shell.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "sfx_shell.wav",
  "type": 0,
  "volume": 1.0,
}