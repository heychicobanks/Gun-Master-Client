{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_arena_theme",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 145.8155,
  "parent": {
    "name": "Theme",
    "path": "folders/Sons/Arenas/TheCity/Theme.yy",
  },
  "preload": false,
  "sampleRate": 48000,
  "soundFile": "sfx_arena_theme.mp3",
  "type": 1,
  "volume": 0.8,
}