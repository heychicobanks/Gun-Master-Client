{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_sheriff_dodge_2",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 1.384626,
  "parent": {
    "name": "Dodge",
    "path": "folders/Sons/Characters/Sheriff/Attack/Dodge.yy",
  },
  "preload": false,
  "sampleRate": 48000,
  "soundFile": "sfx_sheriff_dodge_2.wav",
  "type": 0,
  "volume": 1.0,
}