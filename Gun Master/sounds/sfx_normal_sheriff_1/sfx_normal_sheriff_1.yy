{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_normal_sheriff_1",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 0,
  "conversionMode": 0,
  "duration": 2.163673,
  "parent": {
    "name": "Normal",
    "path": "folders/Sons/Characters/Sheriff/Attack/Normal.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "sfx_normal_sheriff_1.wav",
  "type": 0,
  "volume": 1.0,
}