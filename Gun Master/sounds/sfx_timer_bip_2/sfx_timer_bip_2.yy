{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_timer_bip_2",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 0.232091,
  "parent": {
    "name": "Timer",
    "path": "folders/Sons/Arenas/Misc/Timer.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "sfx_timer_bip_2.wav",
  "type": 1,
  "volume": 1.0,
}