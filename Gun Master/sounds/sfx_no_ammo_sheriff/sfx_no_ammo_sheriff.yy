{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "sfx_no_ammo_sheriff",
  "audioGroupId": {
    "name": "arenaSfxLayer",
    "path": "audiogroups/arenaSfxLayer",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 0.528,
  "parent": {
    "name": "Empty",
    "path": "folders/Sons/Characters/Sheriff/Attack/Empty.yy",
  },
  "preload": false,
  "sampleRate": 48000,
  "soundFile": "sfx_no_ammo_sheriff.mp3",
  "type": 0,
  "volume": 1.0,
}