
if (image_alpha == 0)
    instance_destroy();

 if (z > 0) {
	image_angle += random_range(2,18) * direction;
    z += zspeed;
    zspeed += zgravity;
}
if (z < 0) {
	image_angle = 0;
    z = -z;
	audio_play_sound(sfx_shell, 1, false);
    zspeed = abs(zspeed) * 0.7 - 0.1;
    if (zspeed < 0.75) {
		alarm[1] = random_range(20,30) * 2;
		alarm[0] = 120
        z = 0;
        zspeed = 0;
    }
}

