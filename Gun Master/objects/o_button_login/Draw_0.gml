/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

draw_set_alpha(active ? 1 : 0.65);

draw_self();

// Draw text
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

draw_set_color(c_black);
draw_text(x, y, placeholder);
draw_set_color(c_white);

draw_set_halign(fa_left);
draw_set_valign(fa_top);

draw_set_alpha(1);
