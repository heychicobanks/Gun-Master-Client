/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

if (!active)
	exit;
	
var username = o_input_text.value;
var password = o_input_password.value;

send(PacketType.Login, { "username": username, "password": password});

active = false; // Permet de ne pas envoyer plusieurs fois

with (o_input_username) {
	active = false;
}
with (o_input_password) {
	active = false;
}