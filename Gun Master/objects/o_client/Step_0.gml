/// @description 

if (global.base_socket.state == -1) { // La connexion est perdue
	if (!loader_exists) {
		var msg = "Connecting to server ";
		loader = instance_create_layer(room_width / 2 - string_length(msg) / 2, room_height / 2, "Instances", o_loading_display);
		loader.txt = msg;
		loader_exists = true;
	}
	if (!has_retried) { // On tente de se reconnecter
		//alarm_set(1, retry_increment);
		has_retried = true;
	}
	exit;
}

if (loader_exists) {
	instance_destroy(loader);
	loader_exists = false;
}