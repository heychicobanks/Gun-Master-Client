/// @description Manage server's responses

switch(async_load[? "type"]) {
	case network_type_data:
		var buffer = async_load[? "buffer"];    
		decode_packet(buffer);
		break;
}