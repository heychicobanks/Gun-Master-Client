/// @description Reconnect to server

retry_count++;

if (retry_count > max_connection_retry) {
	show_debug_message("Max connection retry reached");
	exit;
}

show_debug_message("Connecting to server...");

var temp_socket_state;

// On détruit la socket existante
if (global.socket != -1) {
	network_destroy(global.socket);
}

global.socket = network_create_socket_ext(network_socket_tcp, SERVER_PORT);

temp_socket_state = network_connect_raw(global.socket, SERVER_HOST, SERVER_PORT);

if (temp_socket_state == -1) {
	show_debug_message("Reconnexion failed. Retrying...")
	alarm_set(1, retry_increment * retry_count);
} else {
	show_debug_message("Successfully reconnected");
	global.client.has_handshake = false;
	alarm_set(0, room_speed); // Cycle de ping
	retry_count = 0;
	has_retried = false;
}