/// @description
// Vous pouvez écrire votre code dans cet éditeur

#region VARIABLES

// Afin d'éviter d'utiliser le meme seed 

randomise();

// Définition des variables globales

global.client = {
	id : undefined, // ID serveur du client
	username : "Guest", // Nom du joueur
	state: ClientState.HANDSHAKE, // Etat du joueur
	experience: 0, // Experience du joueur
	level : 0, // Niveau du joueur
	rank : UNRANKED, // Rang du joueur
	character_id: 0, // Personnage sélectionné
	unlocked_characters: [0], // Personnages possédés <-- A récupérer du serveur
	buffer: -1, // Sert à stocker les données serveur non traitées
	is_authenticated: false,// Sert à savoir si le client est authentifié
	tcp_port: irandom_range(CLIENT_TCP_PORT_MIN, CLIENT_TCP_PORT_MAX),
	udp_port: irandom_range(CLIENT_UDP_PORT_MIN, CLIENT_UDP_PORT_MAX)
}

/// A MODIFIER ABSOLUMENT
global.entityMap = ds_map_create();

// Permet de gérer la session

global.token = ""; // Token JWT du serveur
global.refreshToken = ""; // Token JWT de rafraichissement à stocker dans le local storage

// Permet de gérer le ping

display_ping = true;
ping_delay = room_speed * 5 // Toutes les 5 secondes
global.ping = 0;

// Parametre de la socket

has_retried = false; // Flag permettant de vérifier si une tentative de reconnexion est en cours
retry_count = 0; // Permet de connaitre le nombre de fois que le client a essayé de se reconnecter au serveur
max_connection_retry = 3 // Nombre de tentative de connexion au serveur en cas d'échec
retry_increment = room_speed // Temps d'attente entre les reconnexions

global.socket = -1 // ID Socket de connnexion au serveur
global.socket_state = -1 // Permet de stocker la socket de connexion au serveur

global.base_socket = {
	socket : -1,
	state : -1,
}

global.game_socket = {
	socket : -1,
	state : -1,
	ip : undefined,
	port : undefined
}

#endregion

// Définition de la socket

global.base_socket.socket = network_create_socket_ext(network_socket_tcp, global.client.tcp_port);

show_debug_message("tcp port : " + string(global.client.tcp_port) + " socket state = " + string(global.base_socket.socket));

network_set_config(network_config_connect_timeout, 5000);
network_set_config(network_config_use_non_blocking_socket, 1);


global.base_socket.state = network_connect_raw(global.base_socket.socket, SERVER_HOST, SERVER_PORT); // On se connecte au serveur

// On envoie le hanshake
send_handshake();

// On lance le cycle de ping

alarm_set(0, ping_delay)

// Animations

loader = -1 // Stocker l'ID de l'instance du loader
loader_exists = false;
