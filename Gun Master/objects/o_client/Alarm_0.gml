/// @description Ping

if (!global.base_socket.state) // La connexion est perdue
	exit;

if (!display_ping) // On affiche pas le ping
	exit;

send(PacketType.Ping, { "timestamp": string(current_time)});

alarm_set(0, ping_delay); // Répéter l'opération toutes les x secondes