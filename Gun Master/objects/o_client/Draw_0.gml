/// @description Display ping
// Vous pouvez écrire votre code dans cet éditeur

if (!display_ping)
	exit;

draw_set_color(c_black);

//draw_text(20, 10, actual_ping);
draw_text(10, 10, "Ping : " + string(global.ping) + " ms");