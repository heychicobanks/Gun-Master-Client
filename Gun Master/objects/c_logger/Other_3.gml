exit;
/// @description Save all logs written to the console
//formatting the date
var _month = string(current_month);
if (current_month < 10) {
	_month = "0"+_month;
}
var _hour = string(current_hour);
if (current_hour < 10) {
	_hour = "0"+_hour;
}
var _minute = string(current_minute);
if (current_minute < 10) {
	_minute = "0"+_minute;
}
var _second = string(current_second);
if (current_second < 10) {
	_second = "0"+_second;
}
//saving the logs with the date in the filename
var _filename = string(current_day)+"-"+_month+"-"+string(current_year)+"_"+_hour+"-"+_minute+"-"+_second+"_"+"MyGameName_log.txt";
//e.g. 24-11-2020_13-21-05_MyGameName_log.txt
if (iglc_save(working_directory + _filename) == -1) {
	//file can't be created (because of an illegal filename, for example)
}

//for Windows, files saved using example above will be located by default in: C:\Users\[your Windows username]\AppData\Local\[your game name]