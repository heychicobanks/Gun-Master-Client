/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

//_origin_ID = undefined;

event_inherited();

isInitialized = false;

initialize = function(_id,_projectile_source_id, _attack_type, _move_direction) {
	show_debug_message("Initializing projectile");

	var _type;

	if (_attack_type == CharacterState.ATTACK)
		_type = "attack";
	else if (_attack_type == CharacterState.QUICK_ATTACK)
		_type = "quick_attack";
	else if (_attack_type == CharacterState.SPECIAL_ATTACK)
		_type = "special_attack";
	
	_source_id = _projectile_source_id; // Permet de vérifier la colision
	
	// Inflige des dégats uniquement si c'est en local
	_damage = 0;
	
	_speed = global.projectiles[? _id][? _type][? "speed"];
	
	_sfx_on_dodge = global.projectiles[? _id][? _type][? "sounds"][? "on_dodge"];
	_sfx_on_hit = global.projectiles[? _id][? _type][? "sounds"][? "on_hit"];
	_sfx_on_impact = global.projectiles[? _id][? _type][? "sounds"][? "on_impact"];
	
	_scr_behavior = asset_get_index(global.projectiles[? _id][? _type][? "scripts"][? "behavior"]);
	_scr_particles_on_create = asset_get_index(global.projectiles[? _id][? _type][? "scripts"][? "particles"][? "on_create"]);
	_scr_particles_trailing = asset_get_index(global.projectiles[? _id][? _type][? "scripts"][? "particles"][? "trailing"]);
	_scr_particles_on_destroy = asset_get_index(global.projectiles[? _id][? _type][? "scripts"][? "particles"][? "on_destroy"]);
	
	image_speed = asset_get_index(global.projectiles[? _id][? _type][? "image_speed"]);
	sprite_index = asset_get_index(global.projectiles[? _id][? _type][? "sprite"]);
	alarm[0] = 30; // Détruie le projectile

	_direction = _move_direction; // A définir par le parent
	dodge_played = false;
	hit_played = false;

	// Pour les particules
	
	x_origin = x;
	y_origin = y;
	
	isInitialized = true;
}