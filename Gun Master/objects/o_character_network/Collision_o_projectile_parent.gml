/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

if (!initialized || _state = CharacterState.DEAD || other._source_id == self.id || global.roundState != RoundState.ACTION)
	exit;

switch (_state) {
	case (CharacterState.DEFEND):
		var _sfx_index = irandom_range(0, ds_list_size(other._sfx_on_dodge) - 1);
		var _sfx_on_dodge = asset_get_index(other._sfx_on_dodge[| _sfx_index]);
		
		audio_play_sound(_sfx_on_dodge, 1, false);
		break;
	case (CharacterState.DODGE):
		var _sfx_index = irandom_range(0, ds_list_size(other._sfx_on_dodge) - 1);
		var _sfx_on_dodge = asset_get_index(other._sfx_on_dodge[| _sfx_index]);
		
		audio_play_sound(_sfx_on_dodge, 1, false);
		break;
	default:
		var _sfx_index = irandom_range(0, ds_list_size(other._sfx_on_hit) - 1);
		var _sfx_on_hit = asset_get_index(other._sfx_on_hit[| _sfx_index]);

		_hp -= other._damage;
		uc_shake(other._damage*2.5, 0.2);
		audio_play_sound(_sfx_on_hit, 1, false);
		instance_destroy(other);
		
		for(i = 0; i < other._damage; i += 1)
            instance_create_layer(x,y, "Instances", o_heart_pop_up);
		break;
}