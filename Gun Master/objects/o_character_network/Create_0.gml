/// @description Insérez la description ici

event_inherited();


// Définition des composants de bases pour le multijoueur

init_character_base_components();

// Définition des états 

initialized = false;

initialize = function(character_id, _character_position, is_player = true) {
	// Id du joueur
	_character_id = character_id
	
	// Côté du joueur
	_position = _character_position;
	
	// Direction que regarde le joueur
	_facing_direction = _position == POSITION.LEFT ? Direction.RIGHT : Direction.LEFT;

	// Définition ID projectile

	_projectile_id = global.characters[| _character_id][? "projectile_id"];

	// Définition des icones

	_hp_sprite_index = asset_get_index(global.characters[| _character_id][? "sprites"][? "hp_icon"]);
	_ammo_sprite_index = asset_get_index(global.characters[| _character_id][? "sprites"][? "ammo_icon"]);

	// Définition des stats

	_max_hp = global.characters[| _character_id][? "max_hp"];
	_damage = global.characters[| _character_id][? "damage"];
	_max_ammo = global.characters[| _character_id][? "max_ammo"];
	_max_defend = global.characters[| _character_id][? "defense"];
	_recoil = global.characters[| _character_id][? "recoil"];
	_dodge_frames = global.characters[| _character_id][? "dodge_frames"];
	_max_steps = global.characters[| _character_id][? "max_steps"];
	_step_distance = global.characters[| _character_id][? "step_distance"];
	
	// Définition des scripts de display
	
	_scr_display_hp = asset_get_index(global.characters[| _character_id][? "scripts"][? "display"][? "hp"]);
	_scr_display_ammo = asset_get_index(global.characters[| _character_id][? "scripts"][? "display"][? "ammo"]);
	
	// Définition des sprites relatifs au personnage
	
	_s_idle = asset_get_index(global.characters[| _character_id][? "sprites"][? "idle"][| 0]);
	_s_dead = asset_get_index(global.characters[| _character_id][? "sprites"][? "dead"][| 0]);
	_s_reload = asset_get_index(global.characters[| _character_id][? "sprites"][? "reload"][| 0]);
	_s_attack = asset_get_index(global.characters[| _character_id][? "sprites"][? "attack"][? "sprite"][| 0]);
	_s_defend = asset_get_index(global.characters[| _character_id][? "sprites"][? "defend"][| 0]);
	_s_dodge = asset_get_index(global.characters[| _character_id][? "sprites"][? "defend"][| 0]);
	_s_quick_attack = asset_get_index(global.characters[| _character_id][? "sprites"][? "quick_attack"][? "sprite"][| 0]);
	_s_charge = asset_get_index(global.characters[| _character_id][? "sprites"][? "charge"][| 0]);
	_s_special = asset_get_index(global.characters[| _character_id][? "sprites"][? "special"][| 0]);
	_s_special_attack = asset_get_index(global.characters[| _character_id][? "sprites"][? "special_attack"][| 0]);
	_s_special_defend = asset_get_index(global.characters[| _character_id][? "sprites"][? "special_defend"][| 0]);
	_s_ultimate = asset_get_index(global.characters[| _character_id][? "sprites"][? "ultimate"][| 0]);
	
	//_s_special = asset_get_index(global.characters[| _character_id][? "sprites"][? "Special"][| 0]);
	
	// Récupération des origines de projectiles de chaque états d'attaque
	
	_s_attack_origin_x = global.characters[| _character_id][? "sprites"][? "attack"][? "origin_x"]
	_s_attack_origin_y = global.characters[| _character_id][? "sprites"][? "attack"][? "origin_y"]

	_s_quick_attack_origin_x = global.characters[| _character_id][? "sprites"][? "quick_attack"][? "origin_x"]
	_s_quick_attack_origin_y = global.characters[| _character_id][? "sprites"][? "quick_attack"][? "origin_y"]
	
	// Définition des sons relatifs au personnage (ds_list)
	
	_sfx_reload = global.characters[| _character_id][? "sounds"][? "reload"];

	_sfx_dodge = global.characters[| _character_id][? "sounds"][? "dodge"];
	_sfx_defend = global.characters[| _character_id][? "sounds"][? "defend"];


	_sfx_attack_impact = global.characters[| _character_id][? "sounds"][? "attack"][? "impact"];
	_sfx_attack_quick = global.characters[| _character_id][? "sounds"][? "attack"][? "quick"];
	_sfx_attack_normal = global.characters[| _character_id][? "sounds"][? "attack"][? "normal"];
	_sfx_attack_empty = global.characters[| _character_id][? "sounds"][? "attack"][? "empty"];

	//_sfx_attack_special = asset_get_index(global.characters[? 0][? "sounds"][? "attack"][? "Special"]);
	 
	_scr_init_custom_vars = asset_get_index(global.characters[| _character_id][? "scripts"][? "init"]);

	// Définition des conditions

	_scr_attack_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "attack"][? "check"]);
	_scr_defend_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "defend"][? "check"]);
	_scr_reload_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "reload"][? "check"]);
	_scr_move_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "move"][? "check"]);
	_scr_dodge_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "dodge"][? "check"]);
	_scr_quick_attack_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "quick_attack"][? "check"]);
	_scr_charge_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "charge"][? "check"]);
	_scr_special_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "special"][? "check"]);
	_scr_special_attack_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "special_attack"][? "check"]);
	_scr_special_defend_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "special_defend"][? "check"]);
	_scr_ultimate_checker = asset_get_index(global.characters[| _character_id][? "scripts"][? "ultimate"][? "check"]);

	// Définition des scripts
	
	_scr_idle = asset_get_index(global.characters[| _character_id][? "scripts"][? "idle"]);
	_scr_dead = asset_get_index(global.characters[| _character_id][? "scripts"][? "dead"]);
	_scr_reload = asset_get_index(global.characters[| _character_id][? "scripts"][? "reload"][? "execute"]);
	_scr_attack = asset_get_index(global.characters[| _character_id][? "scripts"][? "attack"][? "execute"]);
	_scr_defend = asset_get_index(global.characters[| _character_id][? "scripts"][? "defend"][? "execute"]);
	_scr_dodge = asset_get_index(global.characters[| _character_id][? "scripts"][? "dodge"][? "execute"]);
	_scr_quick_attack = asset_get_index(global.characters[| _character_id][? "scripts"][? "quick_attack"][? "execute"]);
	_scr_charge = asset_get_index(global.characters[| _character_id][? "scripts"][? "charge"][? "execute"]);
	_scr_special = asset_get_index(global.characters[| _character_id][? "scripts"][? "special"][? "execute"]);
	_scr_special_attack = asset_get_index(global.characters[| _character_id][? "scripts"][? "special_attack"][? "execute"]);
	_scr_special_defend = asset_get_index(global.characters[| _character_id][? "scripts"][? "special_defend"][? "execute"]);
	_scr_ultimate = asset_get_index(global.characters[| _character_id][? "scripts"][? "ultimate"][? "execute"]);
	
	// Gestion des variables en phase 1
	
	_scr_reset_vars = asset_get_index(global.characters[| _character_id][? "scripts"][? "reset"]);
	
	// Affichage des vies
	
	_stats_hud = instance_create_layer(0, 0, "Instances", o_character_hud);
	
	_stats_hud._position = _position;
	_stats_hud.character_id = self;
	_stats_hud._scr_display_hp = _scr_display_hp;
	_stats_hud._scr_display_ammo = _scr_display_ammo;
	
	// Initialisation des particules;
	
	init_character_particles();
	
	// Initialisation des variables locales
	
	init_default_vars()
	_scr_init_custom_vars();
	
	// Affichage des boutons
	
	if (is_player) {
		create_button(0, self);
		create_button(1, self);
		create_button(2, self);	
	}
	
	initialized = true;
}

// caméra

uc_add_instance_following_list(self); 