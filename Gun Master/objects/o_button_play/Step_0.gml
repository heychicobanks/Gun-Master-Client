/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

if (global.client.state == ClientState.IDLE) {
	if (loader_exists) {
		instance_destroy(loader);
		loader_exists = false;
	}
	placeholder = "Play";
} else if (global.client.state == ClientState.INQUEUE) {
	if (!loader_exists) {
		var msg = "Waiting for a game ";
		loader = instance_create_layer(x - string_width(msg) / 2, y - 80, "Instances", o_loading_display);
		loader.txt = msg
		loader_exists = true;
	}
	placeholder = "Cancel";
}

