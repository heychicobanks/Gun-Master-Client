/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

if (global.client.state == ClientState.IDLE) {
	// TODO: utiliser le bon mode
	send(PacketType.Queue, {mode: "Normal"})
} else if (global.client.state == ClientState.INQUEUE) {
	send(PacketType.Dequeue, {})
} else {
	show_debug_message("Cannot perform matchmaking request : Client state not ready");
}