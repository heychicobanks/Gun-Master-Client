/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur
/*
if (global.roundCount < _level_1) {
	uc_bars(false);
} else {
	uc_bars(true);
}
*/

show_debug_message("Timer: " + string(global.countdown));

switch (global.roundState) {
    case RoundState.INIT:
		break;
	case RoundState.PREPARATION:
		global.countdown -= global.dt_steady * global.dt_scale;
		var current_ceil_countdown = ceil(global.countdown);

	    if (current_ceil_countdown != _last_countdown && current_ceil_countdown > 0)
			audio_play_sound(sfx_timer_bip_1, 1, false);

	    _last_countdown = current_ceil_countdown;
		if (global.countdown > 0)
			break;
		audio_play_sound(sfx_timer_bip_2, 1, false);
	    break;
    case RoundState.ACTION:
		_last_countdown = -1;
		break
    case RoundState.ROUND_END:
		_isInitialized = false;
        break;
	default:
		show_debug_message("Default");
	break;
}
