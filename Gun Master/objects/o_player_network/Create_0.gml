/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

// Inherit the parent event

event_inherited();

position_buffer = ds_queue_create(); // Permet de stocker chaque déplacement effectuer par le joueur
	
updatePositionBuffer(position_buffer, x, y); // On ajoute la position initiale

_combo_delay_time = 10; 
_cached_state = CharacterState.IDLE;