/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

player_input_manager();

// Événement Step dans o_character_parent

if (!initialized)
	exit;
	
event_inherited();

depth = -bbox_bottom

if (_hp <= 0) {
	_state = CharacterState.DEAD
	_scr_dead(self);
	exit;
}

//apply_easing(self.id, 1, "linear", 0.25); 

switch (global.roundState) {
	case (RoundState.INIT):
		if (_hasExecuted)
			break;
		_hasExecuted = true;
	case (RoundState.PREPARATION):
		func_idle(self.id);
		reset_default_vars();
		_scr_reset_vars();
		break;
	case (RoundState.ACTION):
		if (_hasExecuted)
			break;
		_hasExecuted = true;
		_state = _chosen_state;
		_chosen_state = CharacterState.IDLE;
		if (_state != CharacterState.IDLE)
			create_depth(self.id, _depth_factor);
		switch (_state) {
			case CharacterState.RELOAD:
				_scr_reload(self.id);
				send(PacketType.Input, { "input": InputType.RELOAD});
				break;
			case CharacterState.DEFEND:
				_scr_defend(self.id);
				send(PacketType.Input, { "input": InputType.DEFEND});
				break;
			case CharacterState.ATTACK:
				_scr_attack(self.id);
				send(PacketType.Input, { "input": InputType.BASIC_ATTACK});
				break;
			case CharacterState.QUICK_ATTACK:
				_scr_quick_attack(self.id);
				send(PacketType.Input, { "input": InputType.QUICK_ATTACK});
				break;
			case CharacterState.DODGE:
				_scr_dodge(self.id);
				send(PacketType.Input, { "input": InputType.DODGE});
				break;
			case CharacterState.CHARGE:
				_scr_charge(self.id);
				break;				
			case CharacterState.SPECIAL_DEFEND:
				_scr_special_defend(self.id);
				send(PacketType.Input, { "input": InputType.SPECIAL_DEFEND});
				break;
			case CharacterState.SPECIAL_ATTACK:
				_scr_special_attack(self.id);
				send(PacketType.Input, { "input": InputType.SPECIAL_ATTACK});
				break;
			case CharacterState.SPECIAL:
				_scr_special(self.id);
				send(PacketType.Input, { "input": InputType.SPECIAL_RELOAD});
				break;
			case CharacterState.IDLE:
			default:
				_scr_idle(self.id);
        break;
		}
	break
}