/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur
/*
if (global.roundCount < _level_1) {
	uc_bars(false);
} else {
	uc_bars(true);
}
*/

switch (global.roundState) {
    case RoundState.INIT:
		if (!_isInitialized) {
			global.countdown = _countdown_default;
			_last_countdown = -1
			_isInitialized = true;
		}
		break;
	case RoundState.PREPARATION:
		if (global.countdown <= 0)
			break;
		global.countdown -= global.dt_steady * global.scale;
		var current_ceil_countdown = ceil(global.countdown);

	    //if (current_ceil_countdown != _last_countdown && current_ceil_countdown > 0 && global.timer_display == TIMER_DISPLAY.STANDARD)
		//	audio_play_sound(sfx_timer_bip_1, 1, false);

	    _last_countdown = current_ceil_countdown;
		if (global.countdown > 0)
			break;
		//audio_play_sound(sfx_timer_bip_2, 1, false);
		global.pause = _pause_default;
		global.roundState = RoundState.ACTION;
	    break;
    case RoundState.ACTION:
			global.countdown = 0
			_last_countdown = -1;
			/*
			global.pause -= global.dt_steady;
		
			if (global.pause > 0 || (instance_exists(o_projectile_parent) && global.timer_mode == TIMER_MODE.IRREGULAR))
				break;
			_last_countdown = -1;
			global.roundCount += 1;
			global.countdown = _countdown_default;
			if (!_isBlocked) {
				global.roundState = RoundState.PREP;
				global.dt_scale += _speed_increment;
				global.dt_scale = clamp(global.dt_scale, 0, _max_speed);
				iglc_write(0, "Round " + string(global.roundCount));
			}*/
        break
    case RoundState.ROUND_END:
		_isInitialized = false;
		global.roundState = RoundState.INIT;
        break;
	case RoundState.GAME_END:
		room_goto(r_main);
		break;
	default:
		show_debug_message("Default");
		break;
}
