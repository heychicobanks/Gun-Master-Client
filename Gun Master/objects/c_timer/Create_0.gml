/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

// Compte à rebours


_last_countdown = -1;

_countdown_default = 3;
_pause_default = _countdown_default / 2;

global.roundCount = 0;
global.roundState = RoundState.INIT
global.pause = _pause_default;
global.countdown = _countdown_default;
global.dt_scale = 1;

global.timer_mode = TIMER_MODE.REGULAR;
global.timer_display = TIMER_DISPLAY.STANDARD;


_level_1 = 6;
_level_2 = 12;


// Initialisez l'alarme pour la première fois

_isInitialized = false;

_isBlocked = false;