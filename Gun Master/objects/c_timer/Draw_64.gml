/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

if (global.timer_display == TIMER_DISPLAY.INVISIBLE)
	exit;

var _pos_x;
var _pos_y;
var _color;
var _text_size;
var _second;
var _display;

if (global.roundCount < _level_1) {
	_color = c_black;
} else if (global.roundCount > _level_1 && global.roundCount < _level_2) {
	_color = c_orange;
} else {
	_color = c_red;
}

draw_set_font(fnt_timer);

_second = global.timer_display == TIMER_DISPLAY.SUSPENSE || global.roundState == RoundState.ROUND_END ? "--" : string(ceil(global.countdown));
_display = global.countdown > 0 ? _second : (TIMER_DISPLAY.SUSPENSE ? "GO" : "--");
_text_size = string_width(_display)

_pos_y = 70;
_pos_x = display_get_gui_width() / 2 ;


//scribble("[pulse][c_black]" + _display + "[c_black][/pulse]").draw(_pos_x, _pos_y);
draw_text_color(_pos_x, _pos_y, _display, _color, _color, _color, _color, image_alpha)

draw_set_font(-1); // Le font princpal