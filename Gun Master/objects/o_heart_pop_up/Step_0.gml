image_alpha -= 0.03
if (image_alpha == 0)
    instance_destroy();

 if (z > 0) {
    z += zspeed;
    zspeed += zgravity;
}
if (z < 0) {
    z = -z;
    zspeed = abs(zspeed) * 0.6 - 0.2;
    if (zspeed < 0.75) {
		alarm[0] = 60
        z = 0;
        zspeed = 0;
    }
}