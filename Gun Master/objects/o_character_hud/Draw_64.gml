/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

// Si il n'y a pas de position indiquée, on affiche rien

if (_position == undefined || character_id == undefined)
	exit;

// Définition des caractéristiques

var xoffset = 156;

var display_pos_y = view_yport[0];
var display_pos_x = (_position == POSITION.LEFT) ? view_xport[0]: display_get_gui_width();

_scr_display_hp(character_id, display_pos_x, display_pos_y, xoffset, (_position == POSITION.LEFT) ? 1 : -1);
_scr_display_ammo(character_id, display_pos_x, display_pos_y, xoffset, (_position == POSITION.LEFT) ? 1 : -1);