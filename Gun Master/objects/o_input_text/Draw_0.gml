/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

var margin;

margin = 4;

draw_set_alpha(active ? 1 : 0.65);

draw_set_circle_precision(64);

draw_set_color(focused && active ? c_red : c_gray)
draw_roundrect_ext(x - width / 2 - margin, y - height / 2 - margin, x + width / 2 + margin, y + height / 2 + margin, 8, 8, false);
draw_set_color(c_white)
draw_roundrect_ext(x - width / 2, y - height / 2, x + width / 2, y + height / 2, 10, 10, false);
draw_set_color(c_black);
draw_text(x - width / 2 + margin, y - height / 2 + margin, string_length(value) <= 0 ? placeholder : value);

draw_set_circle_precision(4);

draw_set_alpha(1);