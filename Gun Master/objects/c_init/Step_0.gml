/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur
// Store previous internal delta time

dtPrevious = dt_current;
// Update internal delta time
dt_current = delta_time/1000000;
// Set raw unsteady delta time affected by time scale
global.dt_unsteady = dt_current * global.dt_scale;

// Prevent delta time from exhibiting sporadic behaviour
if (dt_current > 1/minFPS)
{
    if (dtRestored) 
    { 
        dt_current = 1/minFPS; 
    } 
    else 
    { 
        dt_current = dtPrevious;
        dtRestored = true;
    }
}
else
{
    dtRestored = false;
}

// Assign internal delta time to global delta time affected by the time scale
global.dt_steady = dt_current * global.dt_scale;

