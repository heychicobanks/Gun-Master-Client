/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

var _lang

_init = false;

_required = ["settings", "characters", "objects", "projectiles"];

show_debug_message("----------Init---------------")

// Charger les fichiers de langues

show_debug_message("Loading Language bundle...")

_lang = os_get_language();

global.translation = loadJSONFromFile((_lang == "fr" ) ? "lang_FR.json" : "lang_EN.json");

show_debug_message("Language bundle loaded : " + _lang )

// Chargement des ressources

show_debug_message("----- Loading ressources ------");

// Charger le fichier JSON des personnages dans une variable

for (var i = 0; i < array_length(_required); i++) {
	var _name = _required[i]
	show_debug_message("Loading " + _name + " ...");	
	var _content = loadJSONFromFile(_name + ".json");

	if (_content == undefined) {
		show_debug_message("Error while loading " + _name + ".json !")
		game_end();
	}
	variable_global_set(_name, _content);
	show_debug_message(_name + " loaded !");
}

show_debug_message("Ressources loaded !")
show_debug_message(json_encode(global.projectiles));
// Pour gérer la génération de nombre aléatoire

randomize();

// Chargement des sons

show_debug_message("Initializing audio... ");

audio_group_load(arenaMusicLayer)
audio_group_load(arenaSfxLayer)

//audio_sound_gain(arenaMusicLayer, (global.settings[? "sounds"][? "music"][? "enabled"]) ? global.settings[? "sounds"][? "music"][? "volume"] : 0, 0);
//audio_sound_gain(arenaSfxLayer, (global.settings[? "sounds"]["sfx"][? "enabled"]) ? global.settings[? "sounds"]["sfx"][? "volume"] : 0, 0);

global.debug_mode = false;

// Delta time, pour prévenir l'instabilité des frames 

global.dt_steady = 0;
global.dt_scale = 1.0;

dt_current = delta_time/1000000;
dt_previous = dt_current;
dtRestored = false;
minFPS = 10;

// Init done

_init = true

show_debug_message("----------Init DONE---------------")