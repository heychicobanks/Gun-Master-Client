/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur


var mouse_x_gui = device_mouse_x_to_gui(0);
var mouse_y_gui = device_mouse_y_to_gui(0);

if (place_meeting(mouse_x_gui, mouse_y_gui, id)) {
		if (mouse_check_button_pressed(mb_left))
			if (_can) {
				dispatchEvent(_button_type);
			}
}


if (_player_id._state != CharacterState.DEAD) {
	if (global.roundState == RoundState.PREPARATION)
		_can = _check_script[0](_player_id);
	else if (global.roundState == RoundState.ACTION) {
		if (_player_id._chosen_state == CharacterState.CHARGE && _button_type == "Reload")
			_can = _check_script[2](_player_id);
		else
			_can = _check_script[1](_player_id);	
	}
} else
	_can = false;

// Exemple pour o_button_attack

if (array_contains(_state_to_check, _player_id._chosen_state) && _can) {
	sprite_index = _selected_sprite;
} else if (_can) {
	sprite_index = _default_sprite;
	temp = false;
} else {
	temp = false;
	sprite_index = _disabled_sprite;

}