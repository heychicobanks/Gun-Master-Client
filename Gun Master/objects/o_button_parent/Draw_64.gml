/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

var gui_width, gui_height, offset;

offset = 512;

gui_width = display_get_gui_width();
gui_height = display_get_gui_height();

pos_x =  (((gui_width / 3) * 1.5) + (_button_position * offset)); // 0.5 pour la moitiée
pos_y = gui_height - 150;

draw_sprite_ext(sprite_index, image_index, pos_x, pos_y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);