/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

enum DIFFICULTY {
	EASY,
	MEDIUM,
	HARD,
}

enum BEHAVIOR {
	PEACEFUL,
	NOOB,
	BALANCED,
	AGRESSIVE,
	EXPERT,
	COL_COUNT,
}

// Inherit the parent event
event_inherited();

initialize(0);

_position = Direction.LEFT

_stats_hud._position = _position;
_stats_hud._instance_ID = self;

has_chosen_prep = false;
has_chosen_fight = false;

image_xscale *= _position
